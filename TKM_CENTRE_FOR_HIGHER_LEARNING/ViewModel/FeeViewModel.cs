﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TKM_CENTRE_FOR_HIGHER_LEARNING.Models;

namespace TKM_CENTRE_FOR_HIGHER_LEARNING.ViewModel
{
    public class FeeViewModel
    {
      
        public SelectList student_select_List { get; set; }

        public FeeSettings feeSettingsModel { get; set; }
        public List<FeeSettings> _feeSettingsList { get; set; }

        public string ID { get; set; }
        public string first_name { get; set; }

        public List< FeeModel> _feeList { get; set; }
    }

  
}