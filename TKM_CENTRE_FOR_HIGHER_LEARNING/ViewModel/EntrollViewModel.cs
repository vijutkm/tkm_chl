﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TKM_CENTRE_FOR_HIGHER_LEARNING.Models;

namespace TKM_CENTRE_FOR_HIGHER_LEARNING.ViewModel
{
    public class EntrollViewModel
    {
        public Course course { get; set; }
        public Entroll entroll { get; set; }
        public IList<SelectListItem> StudentNames { get; set; }
        public string student_name { get; set; }
        public string student_id { get; set; }
        public decimal due { get; set; }
        public int due_status { get; set; }
        public List<EntrollViewModel> vmList { get; set; }
    }
}