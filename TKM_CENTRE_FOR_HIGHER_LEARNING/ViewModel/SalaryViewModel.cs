﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TKM_CENTRE_FOR_HIGHER_LEARNING.Models;

namespace TKM_CENTRE_FOR_HIGHER_LEARNING.ViewModel
{
    public class SalaryViewModel
    {
        public List<Salary> _SalaryList { get; set; }
        public Salary salary { get; set; }
        public List<SalaryViewModel> vmList { get; set; }

    }
}