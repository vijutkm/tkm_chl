﻿using Microsoft.Reporting.WebForms;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TKM_CENTRE_FOR_HIGHER_LEARNING.Models;

namespace TKM_CENTRE_FOR_HIGHER_LEARNING.Reports.Pages
{
    public partial class FeeReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)

            {
                try
                {
                    //int ClassId = Convert.ToInt32(Request.QueryString["classId"]);
                    var qs = Request.QueryString["feeModel"];
                    var uri = WebUtility.UrlDecode(Request.QueryString["feeModel"]);
                    var data = JsonConvert.DeserializeObject(uri);
                    dynamic data1 =  JObject.Parse(data.ToString());
                    int courseid=data1.Course_Id;
                    int semester = data1.Semester;
                    DateTime from_date = data1.from_date;
                    DateTime to_date = data1.to_date;
                    //Response.QueryString["feeModel"].ToString();
                    //ReportViewer1.Reset();
                    ReportViewer1.ProcessingMode = ProcessingMode.Local;
                    //ReportViewer1.LocalReport.EnableExternalImages = true;
                    ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports/Report1.rdlc");
                    DataSet ds = new DataSet();
                    dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
                    dal_stored_procedure1.name = "[dbo].[get_student_fee]";
                    dal_manager dal_manager1 = new dal_manager();
                    dal_stored_procedure1.add_parameter("@course_id", courseid, DbType.Int32, ParameterDirection.Input);
                    dal_stored_procedure1.add_parameter("@sem", semester, DbType.Int32, ParameterDirection.Input);
                    dal_stored_procedure1.add_parameter("@fromdate", from_date, DbType.Date, ParameterDirection.Input);
                    dal_stored_procedure1.add_parameter("@todate", to_date, DbType.Date, ParameterDirection.Input);
                    ds = dal_manager1.execute_dataset(dal_stored_procedure1);
                    //ReportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("Dataset1", ds));
                    if (ds.Tables.Count > 0)
                    {
                        ReportDataSource datasource = new ReportDataSource("DataSet1", ds.Tables[0]);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportViewer1.LocalReport.DataSources.Add(datasource);
                    }
                }
                catch (Exception ex)
                {

                    throw ex;
                }
            }
        }
    }
}