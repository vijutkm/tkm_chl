﻿using Microsoft.Reporting.WebForms;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TKM_CENTRE_FOR_HIGHER_LEARNING.Models;

namespace TKM_CENTRE_FOR_HIGHER_LEARNING.Reports.Pages
{
    public partial class SalaryReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)

            {
                try
                {
                    var qs = Request.QueryString["salaryModel"];
                    var uri = WebUtility.UrlDecode(Request.QueryString["salaryModel"]);
                    var data = JsonConvert.DeserializeObject(uri);
                    dynamic data1 = JObject.Parse(data.ToString());
                    int staff_id = data1.staff_id;
                    int month = data1.month;
                    int year = data1.year;
                    ReportViewer1.ProcessingMode = ProcessingMode.Local;
                    ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports/SalaryReport.rdlc");
                    DataSet ds = new DataSet();
                    dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
                    dal_stored_procedure1.name = "[dbo].[get_Salary_Details]";
                    dal_manager dal_manager1 = new dal_manager();
                    dal_stored_procedure1.add_parameter("@staff_id", staff_id, DbType.Int32, ParameterDirection.Input);
                    dal_stored_procedure1.add_parameter("@month", month, DbType.Int32, ParameterDirection.Input);
                    dal_stored_procedure1.add_parameter("@year", year, DbType.Int32, ParameterDirection.Input);
                    ds = dal_manager1.execute_dataset(dal_stored_procedure1);
                    if (ds.Tables.Count > 0)
                    {
                        ReportDataSource datasource = new ReportDataSource("DataSet1", ds.Tables[0]);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportViewer1.LocalReport.DataSources.Add(datasource);
                        //ReportViewer1.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(SalarySubReportProcessing);
                    }
                }
                catch (Exception ex)
                {

                    throw ex;
                }
            }

        }
        
        //void SalarySubReportProcessing(object sender,SubreportProcessingEventArgs e)
        //{

        //}
    }
}