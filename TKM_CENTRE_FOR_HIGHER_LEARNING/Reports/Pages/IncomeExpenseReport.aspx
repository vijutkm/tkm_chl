﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="IncomeExpenseReport.aspx.cs" Inherits="TKM_CENTRE_FOR_HIGHER_LEARNING.Reports.Pages.IncomeExpenseReport" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
   <form id="form1" runat="server">
        <div>            
            <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="100%" BackColor="#CCCCCC" AsyncRendering="False"></rsweb:ReportViewer>
        </div>
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    </form>
</body>
</html>
