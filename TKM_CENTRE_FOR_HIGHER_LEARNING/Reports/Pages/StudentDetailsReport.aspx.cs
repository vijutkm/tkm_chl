﻿using Microsoft.Reporting.WebForms;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TKM_CENTRE_FOR_HIGHER_LEARNING.Models;

namespace TKM_CENTRE_FOR_HIGHER_LEARNING.Reports.Pages
{
    public partial class StudentDetailsReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)

            {
                try
                {
                    var qs = Request.QueryString["studentModel"];
                    var uri = WebUtility.UrlDecode(Request.QueryString["studentModel"]);
                    var data = JsonConvert.DeserializeObject(uri);
                    dynamic data1 = JObject.Parse(data.ToString());
                    int courseid = data1.Course_Id;
                    int semester = data1.Semester;                
                    ReportViewer1.ProcessingMode = ProcessingMode.Local;                    
                    ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports/StudentDetailsReport.rdlc");
                    DataSet ds = new DataSet();
                    dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
                    dal_stored_procedure1.name = "[dbo].[get_student_List]";
                    dal_manager dal_manager1 = new dal_manager();
                    dal_stored_procedure1.add_parameter("@course_id", courseid, DbType.Int32, ParameterDirection.Input);
                    dal_stored_procedure1.add_parameter("@sem", semester, DbType.Int32, ParameterDirection.Input);                   
                    ds = dal_manager1.execute_dataset(dal_stored_procedure1);
                    if (ds.Tables.Count > 0)
                    {
                        ReportDataSource datasource = new ReportDataSource("DataSet1", ds.Tables[0]);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportViewer1.LocalReport.DataSources.Add(datasource);
                    }
                }
                catch (Exception ex)
                {

                    throw ex;
                }
            }

        }
    }
}