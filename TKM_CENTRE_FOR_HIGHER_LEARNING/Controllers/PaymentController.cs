﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TKM_CENTRE_FOR_HIGHER_LEARNING.Models;
using TKM_CENTRE_FOR_HIGHER_LEARNING.ViewModel;

namespace TKM_CENTRE_FOR_HIGHER_LEARNING.Controllers
{
    public class PaymentController : Controller
    {
        // GET: Payment
        public ActionResult Create()
        {


            Payment_Category_DAL objDal = new Payment_Category_DAL();
            TempData["temp_payment_Category_list"] = new SelectList(objDal.payment_Category_list(), "payment_category_id", "category_name");

            return View("Create");
        }
        [HttpPost]
        public ActionResult Create(Payment payment)
        {

            PaymentDAL objP = new PaymentDAL();
            if (objP.save_payment(payment))
            {
                ViewBag.Message = "Added Successfully";
                TempData["message"] = "Added Successfully";
                ModelState.Clear();

            }
            else
            {
                ViewBag.Message = "Fail To Add Details";
                TempData["message"] = "Fail To Add Details";
            }
          
            Payment_Category_DAL objDal = new Payment_Category_DAL();
            TempData["temp_payment_Category_list"] = new SelectList(objDal.payment_Category_list(), "payment_category_id", "category_name");
            return View("Create");
        }
        public ActionResult List()
        {

            PaymentViewModel obj = new PaymentViewModel();
            PaymentDAL objP = new PaymentDAL();
            obj._payment_list = objP.Get_all_payment_List();
            return View(obj);
        }
        public JsonResult payment_details()
        {
            PaymentDAL objDAL = new PaymentDAL();
            DataSet ds;


            ds = objDAL.get_payment_details();

            DataTable dt = ds.Tables[0];
            System.Web.Script.Serialization.JavaScriptSerializer serializer =
            new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows =
               new List<Dictionary<string, object>>();
            Dictionary<string, object> row = null;

            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return Json(rows, JsonRequestBehavior.AllowGet);
        }
        public JsonResult payment_details_chart()
        {
            PaymentDAL objDAL = new PaymentDAL();
            DataSet ds;


            ds = objDAL.get_payment_details();

            DataTable dt = ds.Tables[1];
            System.Web.Script.Serialization.JavaScriptSerializer serializer =
            new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows =
               new List<Dictionary<string, object>>();
            Dictionary<string, object> row = null;

            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return Json(rows, JsonRequestBehavior.AllowGet);
        }

    }
}