﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TKM_CENTRE_FOR_HIGHER_LEARNING.Models;
using static TKM_CENTRE_FOR_HIGHER_LEARNING.MvcApplication;

namespace TKM_CENTRE_FOR_HIGHER_LEARNING.Controllers
{
    public class ParentController : Controller
    {
        public string  stude_id { get; set; }
        ParentModel objp = new ParentModel();
        StudentDal sdb = new StudentDal();
        // GET: Parent
        public ActionResult Create()
        {
           TempData["stude_id"] = TempData["student_id"].ToString();

            //objp.StudentNameList = new SelectList(sdb.StudentList(1), "strStudentId", "strFirstName");

            Relationship_Dal obj_relationship = new Relationship_Dal();

            obj_relationship.RelationshipList = new SelectList(obj_relationship.Realtionship_List(), "id", "Relationship");
            TempData["relationship_list"] = obj_relationship.RelationshipList;


            return View();
        }

        [NoDirectAccess]
        [HttpPost]
        public ActionResult Create(ParentModel Pmodel)
        {

            if(Session["UserID"]!=null)
            {
                int user_id = int.Parse(Session["UserID"].ToString());
               // string student_id = TempData["student_id"].ToString();
                string stu_id = TempData["stude_id"].ToString();
                ParentDal obj = new ParentDal();


                if (obj.AddParent(Pmodel, stu_id, user_id))
                {
                    ViewBag.Message = " Added Successfully";
                    TempData["message"] = " Added Successfully";
                    ModelState.Clear();
                    return RedirectToAction("Liststudent/" + stu_id);
                }

               
            }
            return View();
        }

        

        [NoDirectAccess]
        public ActionResult List()
        {
           
            ParentDal obj = new ParentDal();

            ModelState.Clear();
            return View(obj.ParentList(1));
           // return View(obj.Parent_by_student_id(id).Find(smodel => smodel.student_id == id));

        }

        [NoDirectAccess]
        [HttpGet]
        public ActionResult Liststudent(string id)
        {

            ParentDal obj = new ParentDal();
            TempData["student_id"] = id;
            ModelState.Clear();
            return View("List",obj.Parent_by_student_id(id));

        }

        [NoDirectAccess]

        public ActionResult Edit(string id)
        {
            ParentDal obj = new ParentDal();

            Relationship_Dal obj_relationship = new Relationship_Dal();

            //obj_relationship.RelationshipList = new SelectList(obj_relationship.Realtionship_List(), "id", "Relationship");
            //TempData["relationship_list"] = obj_relationship.RelationshipList;

            int rel_id = obj.Parent_by_studentid(id).id;

            obj_relationship.RelationshipList = new SelectList(obj_relationship.Realtionship_List(), "id", "Relationship",
          obj_relationship.Realtionship_List().Find(smodel => smodel.id == rel_id).id);

            TempData["relationship_list"] = obj_relationship.RelationshipList;

          

            TempData["student_id"] = id;
           // ViewBag.Studentdropdown = new SelectList(sdb.StudentList(1), "strStudentId", "strFirstName", obj.ParentList(1).Find(smodel => smodel.student_id == id).student_id);
            //return View(obj.Parent_by_student_id(student_id).Find(smodel => smodel.student_id == student_id));

            return View(obj.Parent_by_studentid(id));

        }

        [NoDirectAccess]
        [HttpPost]
        public ActionResult Edit(ParentModel Pmodel)
        {
            var student_id = TempData["student_id"];

            ParentDal obj = new ParentDal();
           
            if (obj.update_parent(Pmodel, student_id.ToString()))
            {
                ViewBag.AlertMsg = "Updated Successfully";
                TempData["message"] = "Updated Successfully";
                return RedirectToAction("Liststudent/" + student_id);
            }
            return View();
        }

        [NoDirectAccess]
        public ActionResult Delete(int id)
        {
            try
            {
                ParentDal pdal = new ParentDal();

                if (pdal.delete_parent(id))
                {
                    ViewBag.AlertMsg = "Deleted Successfully";
                    TempData["message"] = "Deleted Successfully";
                }
                return RedirectToAction("Liststudent/" + TempData["student_id"]);
            }
            catch
            {
                return View();
            }
        }

    }
}