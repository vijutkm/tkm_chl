﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TKM_CENTRE_FOR_HIGHER_LEARNING.Models;
using TKM_CENTRE_FOR_HIGHER_LEARNING.ViewModel;

namespace TKM_CENTRE_FOR_HIGHER_LEARNING.Controllers
{
    public class EntrollController : Controller
    {
        EntrollViewModel objStudentModel = new EntrollViewModel();
        StudentDal objDal = new StudentDal();
        Course_Dal obj_course = new Course_Dal();

        // GET: Entroll
        public ActionResult Index()
        {
            List<SelectListItem> listTeam = new List<SelectListItem>();
            listTeam.Add(new SelectListItem());
            obj_course.CourseList = new SelectList(obj_course.Course_List(), "course_id", "course_name");
            TempData["course_list"] = obj_course.CourseList;
           
            objStudentModel.StudentNames = listTeam;
            objStudentModel.vmList = new List<EntrollViewModel>();
            //objDal.GetAllStudentList();


            return View(objStudentModel);

          

            
        }

       
        public ActionResult LoadStudent(EntrollViewModel vm)
        {            


            objStudentModel = objDal.GetAllStudentList(vm);

            
          //  string strDDLValue = Request.Form["course.course_id"].ToString();

            obj_course.CourseList = new SelectList(obj_course.Course_List(), "course_id", "course_name");
            TempData["course_list"] = obj_course.CourseList;
            return View("Index",objStudentModel);

        }
        [HttpPost]
        public ActionResult EntrollStudent(string Name)
        {

            EntrollDAL obj = new EntrollDAL();
            if (obj.entroll_stundent(Name)==1)
            {
                 return Json(new { success = Name });
            }
            else
            {
                return Json(new { success = Name });
            }
           
        }
    }
}