﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TKM_CENTRE_FOR_HIGHER_LEARNING.Models;
using TKM_CENTRE_FOR_HIGHER_LEARNING.ViewModel;

namespace TKM_CENTRE_FOR_HIGHER_LEARNING.Controllers
{
    public class FeeController : Controller
    {
        // GET: Fee
        public ActionResult Index()
        {
            ViewBag.Title = "Fee Home";

            ViewBag.Last_date = DateTime.Now.ToString();
            FeeViewModel _feeViewModel = new FeeViewModel();
          

            StudentDal objDal = new StudentDal();           
            _feeViewModel.student_select_List = new SelectList(objDal.StudentList(1), "strStudentId", "strFirstName");

            TempData["student_list"] = _feeViewModel.student_select_List;
            FeeSettingsDal objDal1 = new FeeSettingsDal();
            _feeViewModel._feeSettingsList = objDal1.Get_FeeSettings_List("F2B7EBA1-2D40-4AEB-BAE8-559D4A61CB41");
            return View(_feeViewModel);
        }

        public ActionResult List()
        {
            string Id = null;
            if (TempData["id"] !=null)
            {
                Id = TempData["id"].ToString();
            }            
            FeeViewModel _feeViewModel = new FeeViewModel();
            FeeDal objF = new FeeDal();

            _feeViewModel._feeList = objF.Get_all_fee_List(Id);

            return View(_feeViewModel);
        }
        public ActionResult GetData(FeeViewModel ID)
        {
            string strDDLValue = Request.Form["ID"].ToString();
            TempData["id"] = strDDLValue;
            ViewBag.Last_date = DateTime.Now.ToString();
            FeeViewModel _feeViewModel = new FeeViewModel();
            FeeSettingsDal objDal = new FeeSettingsDal();
            StudentDal objStdDal = new StudentDal();
            _feeViewModel.student_select_List = new SelectList(objStdDal.StudentList(1), "strStudentId", "strFirstName");

            TempData["student_list"] = _feeViewModel.student_select_List;
            _feeViewModel._feeSettingsList = objDal.Get_FeeSettings_List(strDDLValue);

             return View("index",_feeViewModel);
        }
        public ActionResult GetStudent(String ID)
        {
            
            ViewBag.Last_date = DateTime.Now.ToString();
            TempData["id"] = ID;
            FeeDal objDAL = new FeeDal();
            DataSet ds;
            ds = objDAL.get_student_Fee_status(ID);
            ViewBag.Total_Fee = ds.Tables[0].Rows[0]["Fee"].ToString();
            ViewBag.Remaining_Fee = ds.Tables[0].Rows[0]["Balance"].ToString();
            ViewBag.Paid_amount = ds.Tables[0].Rows[0]["Amount"].ToString();
            ViewBag.Last_date = ds.Tables[0].Rows[0]["created_on"].ToString();

            FeeViewModel _feeViewModel = new FeeViewModel();
            FeeSettingsDal objDal = new FeeSettingsDal();
            StudentDal objStdDal = new StudentDal();
            _feeViewModel.student_select_List = new SelectList(objStdDal.StudentList(1), "strStudentId", "strFirstName");

            TempData["student_list"] = _feeViewModel.student_select_List;
            _feeViewModel._feeSettingsList = objDal.Get_FeeSettings_List(ID);

            return View("index", _feeViewModel);
        }
        public JsonResult SaveFee(string Fee, string fee_settings_id,string student_id,string fineamount,string is_due)
        {

            FeeModel objF = new FeeModel();
            objF.amount =Convert.ToDecimal(Fee);
            objF.fine_amount = Convert.ToDecimal(fineamount);
            objF.fee_settings_id = Convert.ToInt32(fee_settings_id);
            objF.is_due = Convert.ToInt32(is_due);
            objF.student_id= student_id;
            objF.created_by = 1;
            FeeDal objDAL = new FeeDal();
            objDAL.AddFee(objF);
            string message = "Success";
            return Json(message, JsonRequestBehavior.AllowGet);
        }
      
        public JsonResult Fee_paid_details(string ID)
        {
            FeeDal objDAL = new FeeDal();
            DataSet ds;
            

            ds = objDAL.get_student_Fee_status(ID);

            DataTable dt = ds.Tables[0];
            System.Web.Script.Serialization.JavaScriptSerializer serializer =
            new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows =
               new List<Dictionary<string, object>>();
            Dictionary<string, object> row = null;

            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return Json(rows, JsonRequestBehavior.AllowGet);
        }

    }
    }