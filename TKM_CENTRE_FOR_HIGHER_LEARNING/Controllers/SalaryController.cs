﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TKM_CENTRE_FOR_HIGHER_LEARNING.Models;
using TKM_CENTRE_FOR_HIGHER_LEARNING.ViewModel;
using static TKM_CENTRE_FOR_HIGHER_LEARNING.Models.Salary;
using static TKM_CENTRE_FOR_HIGHER_LEARNING.MvcApplication;

namespace TKM_CENTRE_FOR_HIGHER_LEARNING.Controllers
{
    public class SalaryController : Controller
    {

        [NoDirectAccess]
        public ActionResult Create(int id)
        {
            
            //StaffDal objDal = new StaffDal();
            //objDal.GetStaff = new SelectList(objDal.GetStaff(), "department_id", "department_name");
            //TempData["department_list"] = objDal.DepartmentList;
            Salary salary = new Salary();
            salary = salary.GetStaffSalary(id);
            salary.staff_id = id;
            return View(salary);
        }

        [NoDirectAccess]
        [HttpPost]
        public ActionResult Create(Salary salary)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Salary sal = new Salary();
                    if (sal.AddSalary(salary))
                    {

                        ModelState.Clear();
                        ViewBag.Message = "Added Successfully";
                        TempData["message"] = "Added Successfully";
                       // return Content("<script language='javascript' type='text/javascript'>alert('Staff Salary Details Added Successfully...'); window.location='../../Teachingstaff/Teachingview'</script>");
                        return RedirectToAction("../Teachingstaff/Teachingview");
                    }

                }
                //departmentdal objDal = new departmentdal();
                //objDal.DepartmentList = new SelectList(objDal.department_List(), "department_id", "department_name");
                //TempData["department_list"] = objDal.DepartmentList;


                //Roledal objroledal = new Roledal();
                //objroledal.RoleList = new SelectList(objroledal.Role_List(), "role_id", "role_name");
                //TempData["Role_list"] = objroledal.RoleList;

                return View();

            }
            catch (Exception ex)
            {
                return View();
            }
        }


        [NoDirectAccess]
       
        public ActionResult list(Salary list)
        {
            SalaryViewModel salaryview = new SalaryViewModel();
            SalaryDal salary_dal = new SalaryDal();
            salaryview._SalaryList = new List<Salary>();

            ViewBag.Months = new SelectList(Enumerable.Range(1, 12).Select(x =>
              new SelectListItem()
              {
                 Text = CultureInfo.CurrentCulture.DateTimeFormat.AbbreviatedMonthNames[x - 1] + " (" + x + ")",
                 Value = x.ToString()
              }), "Value", "Text");

        

            ViewBag.Years = new SelectList(Enumerable.Range(DateTime.Today.Year, 20).Select(x =>

               new SelectListItem()
               {
                   Text = x.ToString(),
                   Value = x.ToString()
               }), "Value", "Text");



            return View(salaryview);
  
        }

        [HttpPost]
        [NoDirectAccess]
        public ActionResult list_date(SalaryViewModel vm)
        {
            SalaryViewModel salaryview = new SalaryViewModel();
            SalaryDal salary_dal = new SalaryDal();
            salaryview._SalaryList = salary_dal.Get_salary_List(vm);

            ViewBag.Months = new SelectList(Enumerable.Range(1, 12).Select(x =>
              new SelectListItem()
              {
                  Text = CultureInfo.CurrentCulture.DateTimeFormat.AbbreviatedMonthNames[x - 1] + " (" + x + ")",
                  Value = x.ToString()
              }), "Value", "Text");


            ViewBag.Years = new SelectList(Enumerable.Range(DateTime.Today.Year, 20).Select(x =>

               new SelectListItem()
               {
                   Text = x.ToString(),
                   Value = x.ToString()
               }), "Value", "Text");

        

            return View("list",salaryview);
        }


        [HttpPost]
      
        public ActionResult Savesalary(string[] Name)
        {
            
            int login_user_id = Convert.ToInt32(Session["UserID"]);
            Salary salary = new Salary();
            salary.staff_id = Convert.ToInt32(Name[0]);
            salary.leave_deduction = Convert.ToDecimal(Name[1]);
            salary.salary_advanced_deduction = Convert.ToDecimal(Name[2]);
            salary.tds = Convert.ToDecimal(Name[3]);
            salary.bonus = Convert.ToDecimal(Name[4]);
            salary.gross_salary = Convert.ToDecimal(Name[5]);
            salary.gross_deduction = Convert.ToDecimal(Name[6]);
            salary.net_salary = Convert.ToDecimal(Name[7]);
            salary.SelectedMonth = Convert.ToInt32(Name[8]);
            salary.SelectedYear = Convert.ToInt32(Name[9]);


            try
            {
                if (salary.UpdateSalary(salary, login_user_id))
                    {
                        ModelState.Clear();
                        ViewBag.Message = "Added Successfully";
                    TempData["message"] = "Added Successfully";

                }

                }

            catch (Exception ex)
            {
               
            }

         
            return Json(new { success = Name });
        }



    }
}