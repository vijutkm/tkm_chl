﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using TKM_CENTRE_FOR_HIGHER_LEARNING.Models;
using static TKM_CENTRE_FOR_HIGHER_LEARNING.Models.Login;
using static TKM_CENTRE_FOR_HIGHER_LEARNING.MvcApplication;

namespace TKM_CENTRE_FOR_HIGHER_LEARNING.Controllers
{
 
    public class ForgotpasswordController : Controller
    {
        // GET: Forgotpassword
        public ActionResult Index()
        {
            return View();
        }

        [NoDirectAccess]
        [HttpPost]
        public ActionResult submit(forgetpasswords forgetpassword)
        {

            try
            {
                int user_id = forgetpassword.Validate_number(forgetpassword);

                if (user_id > 0)
                {
                    Session["UserID"] = user_id.ToString();
                    //Session["UserName"] = obj.UserName.ToString();

                    int retrun_value = add_otp(user_id, forgetpassword);
                    if (retrun_value >= 0)
                    {
                        ViewBag.Message = "OTP Send Successfully";
                        Session["phone_number"] = forgetpassword.mob_number.ToString();
                        return RedirectToAction("gotootp");

                    }

                }

                else
                {
                     ViewBag.Message = "Invalid Number or User";
                    return Content("<script language='javascript' type='text/javascript'>alert('Invalid Number...'); window.location='../../'</script>"); 


                }

            }


            catch (Exception ex)
            {
                return View("forgetpassword", "Login");
            }

            return View("Login", "Login");
        }


        [NoDirectAccess]
        [HttpPost]
        public ActionResult gotosubmit(forgetpasswords forgetpassword)
        {
            try
            {
                string ph_number = Session["phone_number"].ToString();
                if (ph_number !="")
                {
   
                    int return_value_staff_id = check_valid_otp(forgetpassword, ph_number);
                    if (return_value_staff_id > 0)
                    {
                       
                            ViewBag.Message = "OTP Verified Successfully";
                             Session["staff_id"] = return_value_staff_id.ToString();
                        //  return RedirectToAction("Login", "Login");
                        return RedirectToAction("gotoforgtpwdview", "Forgotpassword");


                    }
                    else
                    {
                        ViewBag.Message = "Invalid OTP";
                        return View("otp");
                    }
                }

                else
                {
                    ViewBag.Message = "Required";
                }

            }


            catch (Exception ex)
            {
                return View("otp");
            }

            return View("otp");
        }

        [NoDirectAccess]
        [HttpPost]
        public ActionResult gotosubmitfrwd(password_change password_change)
        {
            try
            {
                int staff_id_number = Convert.ToInt32(Session["staff_id"]);
                if (staff_id_number >0)
                {
                    if(password_change.new_password == password_change.confirm_password)
                    {
                        int password_success = add_new_password(password_change.new_password, staff_id_number);
                        if(password_success == 1)
                        {
                            ViewBag.Message = "Password Changed Successfully";
                            return RedirectToAction("Login", "Login");
                        }
                        else
                        {
                            ViewBag.Message = "Password Cannot Be Changed";
                        }
                    }
                    else
                    {
                        ViewBag.Message = "Password Mismatch";

                    }
                  
                }

                else
                {
                    ViewBag.Message = "Staff Id is Null";
                }

            }

            catch (Exception ex)
            {
                return View("forgotpasswordview");
            }

            return View("forgotpasswordview");
        }

     
        public ActionResult gotootp()
        {
          //  FormsAuthentication.SignOut();


            return View("otp");
        }

        [NoDirectAccess]
        public ActionResult gotoforgtpwdview()
        {
            FormsAuthentication.SignOut();


            return View("forgotpasswordview");
        }

        public int add_otp(int user_id, forgetpasswords fobj)
        {
            int return_value = -1;
            try
            {
                dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
                dal_stored_procedure1.name = "[dbo].[pr_add_otp]";
                dal_manager dal_manager1 = new dal_manager();
                dal_stored_procedure1.add_parameter("@user_id", user_id, DbType.Int32, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@mobile_number", fobj.mob_number, DbType.String, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@return_val", null, DbType.Int32, ParameterDirection.Output);

                dal_manager1.execute_nonquery(dal_stored_procedure1);
                return_value = (int)dal_stored_procedure1.get_params()["@return_val"].param_value;
                return return_value;

            }
            catch (Exception ex)
            {

            }
            return return_value;
        }



        public int check_valid_otp( forgetpasswords fobj ,string phone_number)
        {
            int return_value = -1;
            try
            {
                dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
                dal_stored_procedure1.name = "[dbo].[pr_check_valid_otp]";
                dal_manager dal_manager1 = new dal_manager();
                dal_stored_procedure1.add_parameter("@mob_number", phone_number, DbType.String, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@otp", fobj.otp, DbType.Int32, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@return_val", null, DbType.Int32, ParameterDirection.Output);

                dal_manager1.execute_nonquery(dal_stored_procedure1);
                return_value = (int)dal_stored_procedure1.get_params()["@return_val"].param_value;
                return return_value;

            }
            catch (Exception ex)
            {

            }
            return return_value;
        }

        public int add_new_password(string new_password,int staff_id)
        {
            int return_value = -1;
            try
            {
                dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
                dal_stored_procedure1.name = "[dbo].[pr_add_new_password]";
                dal_manager dal_manager1 = new dal_manager();
                dal_stored_procedure1.add_parameter("@password", new_password, DbType.String, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@staff_id", staff_id, DbType.Int32, ParameterDirection.Input);

                dal_stored_procedure1.add_parameter("@return_val", null, DbType.Int32, ParameterDirection.Output);

                dal_manager1.execute_nonquery(dal_stored_procedure1);
                return_value = (int)dal_stored_procedure1.get_params()["@return_val"].param_value;
                return return_value;

            }
            catch (Exception ex)
            {

            }
            return return_value;
        }
    }
}