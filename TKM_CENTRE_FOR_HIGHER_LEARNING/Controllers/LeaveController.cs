﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TKM_CENTRE_FOR_HIGHER_LEARNING.Models;

namespace TKM_CENTRE_FOR_HIGHER_LEARNING.Controllers
{
    public class LeaveController : Controller
    {
        LeaveTypeDal obj_LeaveTypeDal = new LeaveTypeDal();
        LeaveDal obj_Leave = new LeaveDal();
        // GET: Leave
        public ActionResult Index()
        {

            
            TempData["leave_type"]=  new SelectList(obj_LeaveTypeDal.leave_type_List(), "leave_type_id", "leave_type");
            StaffDal obj_StaffDal = new Models.StaffDal();
            TempData["staff"] = new SelectList(obj_StaffDal.GetStaff_by_role(1), "staff_id", "first_name");

            LeaveDal objDAL = new LeaveDal();
            DataSet ds = objDAL.Get_Staff_leave_status(Convert.ToInt32(Session["UserID"]));

            ViewBag.balance=Convert.ToString( ds.Tables[0].Rows[0]["Balance"]);
            ViewBag.Approved = Convert.ToString(ds.Tables[0].Rows[0]["Approved"]);
            ViewBag.Rejected = Convert.ToString(ds.Tables[0].Rows[0]["Rejected"]);
            ViewBag.Applied = Convert.ToString(ds.Tables[0].Rows[0]["Applied"]);

            return View();
        }
        [HttpPost]
        public JsonResult save(string leave_type_id,string number_of_leaves, string emergancy_contact_number, string assigned_to, string start_date, string end_date,string reason,string type)
        {
            TempData["leave_type"] = new SelectList(obj_LeaveTypeDal.leave_type_List(), "leave_type_id", "leave_type");
            StaffDal obj_StaffDal = new Models.StaffDal();
            TempData["staff"] = new SelectList(obj_StaffDal.GetStaff_by_role(1), "staff_id", "first_name");
            Leave objleave = new Leave();
            objleave.leave_type_id =Convert.ToInt32( leave_type_id);
            objleave.number_of_leaves = Convert.ToDecimal(number_of_leaves);
            objleave.emergancy_contact_number = emergancy_contact_number;
            objleave.assigned_to = Convert.ToInt32(assigned_to);
            objleave.start_date = Convert.ToDateTime(start_date);
            objleave.end_date = Convert.ToDateTime(end_date);
            objleave.staff_id = Convert.ToInt32(Session["UserID"]);
            objleave.reason = reason;
            objleave.leave_id = Convert.ToInt32(type);
            if (objleave.staff_id != 0 && objleave.reason!="" && objleave.emergancy_contact_number!="")
            {
                int retval=obj_Leave.save_leave(objleave);
                if (retval == 2)
                {
                    TempData["message"] = "Leave Is Already Availed Or Applied";
                    TempData["messagetype"] = "Error";
                    //return Json(new { success = false, responseText = "Leave is already availed or applied" });
                    return Json("success", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = true });
                }               
            }
            else
            {
                return Json(new { success = false });
            } 

            
        }
        public string Get_staff_leave()
        {
            LeaveDal objDAL = new LeaveDal();
            DataSet ds = objDAL.Get_Staff_leave_List(Convert.ToInt32(Session["UserID"]));

            DataTable dt = ds.Tables[0];
            System.Web.Script.Serialization.JavaScriptSerializer serializer =
            new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows =
               new List<Dictionary<string, object>>();
            Dictionary<string, object> row = null;

            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            //return Json(rows, JsonRequestBehavior.AllowGet);
               return serializer.Serialize(rows);
        }
        public string Get_staff_leave_history()
        {
            LeaveDal objDAL = new LeaveDal();
            DataSet ds = objDAL.Get_Staff_leave_List(Convert.ToInt32(Session["UserID"]));

            DataTable dt = ds.Tables[1];
            System.Web.Script.Serialization.JavaScriptSerializer serializer =
            new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows =
               new List<Dictionary<string, object>>();
            Dictionary<string, object> row = null;

            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            //return Json(rows, JsonRequestBehavior.AllowGet);
            return serializer.Serialize(rows);
        }
    }
}