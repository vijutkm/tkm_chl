﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using TKM_CENTRE_FOR_HIGHER_LEARNING.Models;
using static TKM_CENTRE_FOR_HIGHER_LEARNING.MvcApplication;

namespace TKM_CENTRE_FOR_HIGHER_LEARNING.Controllers
{
    public class TeachingstaffController : Controller
    {

        //View Staff Details
        [NoDirectAccess]
        public ActionResult Teachingview(Teachingmodel list)
        {

            StaffDal staff_dal = new StaffDal();
            List<Teachingmodel> list_staff_list = staff_dal.GetStaff();
            return View(list_staff_list);

        }

        [NoDirectAccess]
        public ActionResult Create()
        {
            //  ViewBag.Message = "Your contact page.";
            departmentdal objDal = new departmentdal();
            objDal.DepartmentList = new SelectList(objDal.department_List(), "department_id", "department_name");
            TempData["department_list"] = objDal.DepartmentList;

            Roledal objroledal = new Roledal();
            objroledal.RoleList = new SelectList(objroledal.Role_List(), "role_id", "role_name");
            TempData["Role_list"] = objroledal.RoleList;

            TitleDal tDal = new TitleDal();
            
                tDal.TitleList = new SelectList(tDal.Title_List(), "title_id", "title_name");
            TempData["title_list"] = tDal.TitleList;


            return View();
        }


        // POST: Student/Create
        [NoDirectAccess]
        [HttpPost]
        public ActionResult Create(Teachingmodel smodel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Teachingmodel sdb = new Teachingmodel();
                    int i = sdb.AddStaff(smodel);

                    if (i > 0)
                    {

                        ModelState.Clear();
                        ViewBag.Message = "Added Successfully";
                        TempData["message"] = "Added Successfully";
                        // return Content("<script language='javascript' type='text/javascript'>alert('Staff Added Successfully...'); window.location='../../Teachingstaff/Teachingview'</script>");
                        return RedirectToAction("Teachingview");
                    }
                    else
                    {
                        TempData["message"] = "Phone Number Exists";
                        return RedirectToAction("Create");
                       // return Content("<script language='javascript' type='text/javascript'>alert('phone number exists'); window.location='../../Teachingstaff/Create'</script>");
                    }
                   
                }
                departmentdal objDal = new departmentdal();
                objDal.DepartmentList = new SelectList(objDal.department_List(), "department_id", "department_name");
                TempData["department_list"] = objDal.DepartmentList;


                Roledal objroledal = new Roledal();
                objroledal.RoleList = new SelectList(objroledal.Role_List(), "role_id", "role_name");
                TempData["Role_list"] = objroledal.RoleList;

                return View();

            }
            catch (Exception ex)
            {
                return View();
            }
        }


        [NoDirectAccess]
        public ActionResult Delete(int id)
        {
            try
            {
                Teachingmodel sdb = new Teachingmodel();
                if (sdb.DeleteStaff(id))
                {
                    ViewBag.Message = "Deleted Successfully";
                    TempData["message"] = "Deleted Successfully";

                }
                return RedirectToAction("Teachingview");
            }
            catch (Exception ex)
            {
                return View();
            }
        }

        [NoDirectAccess]
        public ActionResult Edit(int id)
        {
            Teachingmodel sdb = new Teachingmodel();
            StaffDal staff_dal = new StaffDal();
            departmentdal objDal = new departmentdal();
            Roledal objroledal = new Roledal();

            int department = staff_dal.GetStaff(id).department_id;

            objDal.DepartmentList = new SelectList(objDal.department_List(), "department_id", "department_name",
            objDal.department_List().Find(smodel => smodel.department_id == department).department_id);

            TempData["department_list"] = objDal.DepartmentList;

            int role = staff_dal.GetStaff(id).role_id;

            objroledal.RoleList = new SelectList(objroledal.Role_List(), "role_id", "role_name",
           objroledal.Role_List().Find(smodel => smodel.role_id == role).role_id);

            TempData["role_list"] = objroledal.RoleList;

            return View(staff_dal.GetStaff(id));
        }

        [NoDirectAccess]
        [HttpPost]
        public ActionResult Edit(int id, Teachingmodel smodel)
        {
            try
            {
                Teachingmodel sdb = new Teachingmodel();
                sdb.UpdateDetails(smodel);
                TempData["message"] = "Updated Successfully";
                return RedirectToAction("Teachingview");
            }
            catch (Exception ex)
            {
                return View();
            }
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UploadFile()
        {
            var fileName="";
            if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
            {
                var pic = System.Web.HttpContext.Current.Request.Files["MyImages"];
                if (pic.ContentLength > 0)
                {
                     fileName = Path.GetFileName(pic.FileName);
                    var _ext = Path.GetExtension(pic.FileName);

                  
                    var _comPath = Server.MapPath("/user_images/") + fileName;
                    

                    ViewBag.Msg = _comPath;
                    var path = _comPath;

                    // Saving Image in Original Mode
                    pic.SaveAs(path);

                    // resizing image
                    MemoryStream ms = new MemoryStream();
                    WebImage img = new WebImage(_comPath);

                    if (img.Width > 200)
                        img.Resize(200, 200);
                    img.Save(_comPath);
                    // end resize
                }
            }
            return Json(Convert.ToString(fileName), JsonRequestBehavior.AllowGet);
        }


    }
}