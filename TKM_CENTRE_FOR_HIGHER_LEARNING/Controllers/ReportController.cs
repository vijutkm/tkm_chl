﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TKM_CENTRE_FOR_HIGHER_LEARNING.Models;
using static TKM_CENTRE_FOR_HIGHER_LEARNING.MvcApplication;

namespace TKM_CENTRE_FOR_HIGHER_LEARNING.Controllers
{
    public class ReportController : Controller
    {
        // GET: Report
        [NoDirectAccess]
        public ActionResult List()
        {            
            Course_Dal obj = new Course_Dal();
            obj.CourseList = new SelectList(obj.Course_List(), "course_id", "course_name");
            TempData["course_list"] = obj.CourseList;            
            return View("ReportStudentFeeList");
        }
        [NoDirectAccess]
        [HttpPost]
        public ActionResult Submit(ReportStudentFee feeModel)
        {
            int id = feeModel.Course_Id;
            var queryString = JsonConvert.SerializeObject(feeModel);
            var uri = WebUtility.UrlEncode(queryString);
            return Redirect("/Reports/Pages/FeeReport.aspx?feeModel="+ uri + "");
        }       
        public ActionResult StudentDetailsSubmit(ReportStudentModel studentModel)
        {
            int id = studentModel.Course_Id;
            var queryString = JsonConvert.SerializeObject(studentModel);
            var uri = WebUtility.UrlEncode(queryString);
            return Redirect("/Reports/Pages/StudentDetailsReport.aspx?studentModel=" + uri + "");
        }
        public ActionResult StudentDetails ()
        {
            Course_Dal obj = new Course_Dal();
            obj.CourseList = new SelectList(obj.Course_List(), "course_id", "course_name");
            TempData["course_list"] = obj.CourseList;
            return View("ReportStudentDetails");
        }

        public ActionResult IncomeExpense()
        {
            Payment_Category_DAL objDal = new Payment_Category_DAL();

            //List<SelectListItem> items = new List<SelectListItem>();
            //items.Add(new SelectListItem() { Text = "All", Value = "0" });
            //items.Insert(items.Count, new SelectListItem { Text = "Others", Value = items.Count.ToString() });

            TempData["temp_payment_Category_list"] = new SelectList(objDal.payment_Category_list(), "payment_category_id", "category_name");
            return View("IncomeExpense");
        }
        public ActionResult IncomeExpenseSubmit(ReportExpenseModel expenseModel)
        {            
            var queryString = JsonConvert.SerializeObject(expenseModel);
            var uri = WebUtility.UrlEncode(queryString);
            return Redirect("/Reports/Pages/IncomeExpenseReport.aspx?expenseModel=" + uri + "");
        }

        public ActionResult Salaryprocessing()
        {
            staff_Dal objDal = new staff_Dal();
            TempData["temp_staff_list"] = new SelectList(objDal.staff_List(), "staff_id", "staff_name");

            ViewBag.Months = new SelectList(Enumerable.Range(1, 12).Select(x =>
             new SelectListItem()
             {
                 Text = CultureInfo.CurrentCulture.DateTimeFormat.AbbreviatedMonthGenitiveNames[x - 1] + " (" + x + ")",
                 Value = x.ToString()
             }), "Value", "Text");



            ViewBag.Years = new SelectList(Enumerable.Range(DateTime.Today.Year, 20).Select(x =>

               new SelectListItem()
               {
                   Text = x.ToString(),
                   Value = x.ToString()
               }), "Value", "Text");

            return View("ReportSalarylist");
        }


        
        public ActionResult SalaryprocessingSubmit(ReportSalaryModel SalaryModel)
        {
            var queryString = JsonConvert.SerializeObject(SalaryModel);
            var uri = WebUtility.UrlEncode(queryString);
            return Redirect("/Reports/Pages/SalaryReport.aspx?salaryModel=" + uri + "");
        }

    }
}