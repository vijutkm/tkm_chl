﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TKM_CENTRE_FOR_HIGHER_LEARNING.Models;

namespace TKM_CENTRE_FOR_HIGHER_LEARNING.Controllers
{
    public class FeeSettingsController : Controller
    {
        // GET: FeeSettings
        public ActionResult Create()
        {

            Course_Dal obj_course = new Course_Dal();
            obj_course.CourseList = new SelectList(obj_course.Course_List(), "course_id", "course_name");
            TempData["course_list"] = obj_course.CourseList;


            CategoryDal obj_category = new CategoryDal();
            TempData["category_list"] = new SelectList(obj_category.category_List(), "category_id", "category_name");

            FeeTypeDAL fDal = new FeeTypeDAL();
            TempData["FeeType_list"] = new SelectList(fDal.FeeType_list(), "fee_type_id", "fee_type");

            return View();
        }
        [HttpPost]
        public ActionResult Create(FeeSettings fs)
        {

            FeeSettingsDal fdal = new FeeSettingsDal();


            if (fdal.AddFeeSettings(fs))
            {
                ViewBag.Message = " Details Added Successfully";
                TempData["message"] = " Details Added Successfully";
                ModelState.Clear();

            }

            Course_Dal obj_course = new Course_Dal();
            TempData["course_list"] = new SelectList(obj_course.Course_List(), "course_id", "course_name");


            CategoryDal obj_category = new CategoryDal();
            TempData["category_list"] = new SelectList(obj_category.category_List(), "category_id", "category_name");

            FeeTypeDAL fDal = new FeeTypeDAL();
            TempData["FeeType_list"] = new SelectList(fDal.FeeType_list(), "fee_type_id", "fee_type");


            return View();
        }
        public ActionResult List()
        {

            FeeSettingsDal fdal = new FeeSettingsDal();

            return View(fdal.Get_All_FeeSettings_List());
        }
        public ActionResult Edit(int id)
        {
            FeeSettingsDal obj = new FeeSettingsDal();

            obj.Get_Fee_settings_By_id(id);




            int course_id = obj.Get_Fee_settings_By_id(id).course_id;
            int category_id = obj.Get_Fee_settings_By_id(id).studentCategory.category_id;





            CategoryDal obj_category = new CategoryDal();
            TempData["category_list"] = new SelectList(obj_category.category_List(), "category_id", "category_name",
            obj_category.category_List().Find(smodel => smodel.category_id == category_id).category_id);

            Course_Dal obj_course = new Course_Dal();
            TempData["course_list"] = new SelectList(obj_course.Course_List(), "course_id", "course_name",
            obj_course.Course_List().Find(smodel => smodel.course_id == course_id).course_id);

            FeeTypeDAL fDal = new FeeTypeDAL();
            TempData["FeeType_list"] = new SelectList(fDal.FeeType_list(), "fee_type_id", "fee_type",
            fDal.FeeType_list().Find(smodel => smodel.fee_type_id == obj.Get_Fee_settings_By_id(id).fee_type_id).fee_type_id);




            return View(obj.Get_Fee_settings_By_id(id));

        }
        [HttpPost]
        public ActionResult Edit(int id,FeeSettings fs)
        {

            FeeSettingsDal obj = new FeeSettingsDal();
            obj.UpdateFeeSettings(fs, id);
            return RedirectToAction("List");
        }
        public ActionResult Delete(int id)
        {

            FeeSettingsDal obj = new FeeSettingsDal();
            if (obj.DelateFeeSettings(id))
            {
               
            }
           
                return RedirectToAction("List");
            
        }
    }
}