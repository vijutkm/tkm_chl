﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TKM_CENTRE_FOR_HIGHER_LEARNING.Models;
using static TKM_CENTRE_FOR_HIGHER_LEARNING.MvcApplication;

namespace TKM_CENTRE_FOR_HIGHER_LEARNING.Controllers
{
    public class DashboardController : Controller
    {
        // GET: Dashboard
        [NoDirectAccess]
        public ActionResult Index()
        {
            try
            {
                PaymentDAL objDAL = new PaymentDAL();
                DataSet ds = objDAL.get_payment_details();

                int a = Convert.ToInt32(ds.Tables[0].Rows[0]["Credit"]) - Convert.ToInt32(ds.Tables[0].Rows[0]["Debit"]);
                ViewBag.balance = a;
                DateTime dt = Convert.ToDateTime(ds.Tables[2].Rows[0]["lastupdated"]);
                string DateOnly = dt.ToShortDateString();
                ViewBag.date = DateOnly;
                StaffDal sDAL = new Models.StaffDal();
                Teachingmodel staff_model = sDAL.GetStaff(Convert.ToInt32(Session["UserID"]));
                ViewBag.name = staff_model.first_name;
                ViewBag.email = staff_model.email_id;
                ViewBag.phone = staff_model.phone_number;
                Teachingmodel user = (Teachingmodel)Session["user"];
                ViewBag.category_name = ds.Tables[3].Rows[0]["category_name"].ToString();
                ViewBag.pay_amount = ds.Tables[3].Rows[0]["pay_amount"].ToString();
                ViewBag.last_hours = ds.Tables[4].Rows[0]["last_hours"].ToString();
                ViewBag.description = ds.Tables[4].Rows[0]["description"].ToString();
                ViewBag.category_name = ds.Tables[4].Rows[0]["category_name"].ToString();
                ViewBag.last_hours1 = ds.Tables[4].Rows[1]["last_hours"].ToString();
                ViewBag.description1 = ds.Tables[4].Rows[1]["description"].ToString();
                ViewBag.category_name1 = ds.Tables[4].Rows[1]["category_name"].ToString();
                ViewBag.last_hours2 = ds.Tables[4].Rows[2]["last_hours"].ToString();
                ViewBag.description2 = ds.Tables[4].Rows[2]["description"].ToString();
                ViewBag.category_name2 = ds.Tables[4].Rows[2]["category_name"].ToString();
                if (user.role_id == -1)
                {
                    return View("admin_dashboard");
                }
                else if (user.role_id == 1)
                {
                    return View("principal_dashboard");
                }
                else
                {
                    return View("staff_dashboard");
                }				

            }
            catch (Exception ex)
            {
                Teachingmodel user = (Teachingmodel)Session["user"];
                if (user.role_id == -1)
                {
                    return View("admin_dashboard");
                }
                else if (user.role_id == 1)
                {
                    return View("principal_dashboard");
                }
                else
                {
                    return View("staff_dashboard");
                }
            }
        }


        // For Displaying Chart
        public JsonResult Collection_paid_details()
        {
            collection_fee objDAL = new collection_fee();
            DataSet ds;


            ds = objDAL.collection_Fee_details_for_chart();

            DataTable dt = ds.Tables[0];
            System.Web.Script.Serialization.JavaScriptSerializer serializer =
            new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows =
               new List<Dictionary<string, object>>();
            Dictionary<string, object> row = null;

            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return Json(rows, JsonRequestBehavior.AllowGet);
        }
        public string Get_staff_leave_history()
        {
            LeaveDal objDAL = new LeaveDal();
            DataSet ds = objDAL.Get_Assigned_leave_List(Convert.ToInt32(Session["UserID"]));

            DataTable dt = ds.Tables[0];
            System.Web.Script.Serialization.JavaScriptSerializer serializer =
            new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows =
               new List<Dictionary<string, object>>();
            Dictionary<string, object> row = null;

            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            //return Json(rows, JsonRequestBehavior.AllowGet);
            return serializer.Serialize(rows);
        }
        public JsonResult update_leave(string leave_id, string status, string remark, string staff_id)
        {
            Leave obj = new Models.Leave();
            obj.leave_id = Convert.ToInt32(leave_id);
            obj.leave_status = status;
            obj.remarks = remark;
            obj.staff_id = Convert.ToInt32(staff_id); ;
            obj.assigned_to = Convert.ToInt32(Session["UserID"]);
            LeaveDal objDal = new Models.LeaveDal();
            objDal.update_leave(obj);

            return Json(new { success = true });
        }
    }
}