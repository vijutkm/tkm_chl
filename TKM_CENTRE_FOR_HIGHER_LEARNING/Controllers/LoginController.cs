﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using TKM_CENTRE_FOR_HIGHER_LEARNING.Models;
using static TKM_CENTRE_FOR_HIGHER_LEARNING.Models.Login;
using static TKM_CENTRE_FOR_HIGHER_LEARNING.MvcApplication;

namespace TKM_CENTRE_FOR_HIGHER_LEARNING.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Login()

        {
            FormsAuthentication.SignOut();
            if (TempData["shortMessage"] != null)
            {
                ViewBag.Message = TempData["shortMessage"].ToString();
                TempData.Remove("shortMessage");
            }
            ViewData["Time"] = DateTime.Now.ToString();
            FormsAuthentication.SignOut();
            Session.Abandon();
            return View("Login", new Login());
        }

        // GET: Login

        [HttpPost]
        public ActionResult submit(Login obj)
        {

            try
            {
                var user_id = obj.Validateusers(obj);
                var ip = getip();
                int log_users = obj.log_users(obj, ip);   //log users for success or failure

                if (user_id > 0)
                {
                    Session["UserID"] = user_id.ToString();

                    StaffDal sdal = new StaffDal();
                    Teachingmodel user = sdal.GetStaff(user_id);
                    Session["user"] = user;
                    Session["UserName"] = user.first_name + ',' + user.last_name;

                    return RedirectToAction("Index", "Dashboard");
                }

                else
                {

                    TempData["shortMessage"] = "User Not Exist";

                    return RedirectToAction("Login", "Login");
                }

            }


            catch (Exception ex)
            {
                return View();
            }

            //  return View("login");
        }

        private string getip()
        {
            string hostName = Dns.GetHostName();
            string myIP = Dns.GetHostByName(hostName).AddressList[0].ToString();
            return myIP;
        }

        [NoDirectAccess]
        public ActionResult forgetpassword()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();

            return View("forgetpassword");
        }

        [NoDirectAccess]
        public ActionResult gotologin()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();

            return View("Login");
        }





    }
}