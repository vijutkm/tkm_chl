﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TKM_CENTRE_FOR_HIGHER_LEARNING.Models;
using static TKM_CENTRE_FOR_HIGHER_LEARNING.MvcApplication;

namespace TKM_CENTRE_FOR_HIGHER_LEARNING.Controllers
{
    public class StudentController : Controller
    {
        [NoDirectAccess]
        public ActionResult List()
        {

            StudentDal sdb = new StudentDal();
           
            ModelState.Clear();
             return View(sdb.StudentList(1));
          

        }

        [NoDirectAccess]
        // GET: Student
        public ActionResult Create()
        {
            Course_Dal obj_course = new Course_Dal();
            obj_course.CourseList = new SelectList(obj_course.Course_List(), "course_id", "course_name");


            TempData["course_list"] = obj_course.CourseList;


            CategoryDal obj_category = new CategoryDal();
            obj_category.categoryList = new SelectList(obj_category.category_List(), "category_id", "category_name");


            TempData["category_list"] = obj_category.categoryList;


            return View();

        }

        [NoDirectAccess]
        [HttpPost]
        public ActionResult Create(StudentModel smodel)
        {


            StudentDal sdb = new StudentDal();
            if (sdb.AddStudent(smodel))
            {
                ViewBag.Message = " Added Successfully";
                TempData["message"] = " Added Successfully";
                ModelState.Clear();

            }

            Course_Dal obj_course = new Course_Dal();
            obj_course.CourseList = new SelectList(obj_course.Course_List(), "course_id", "course_name");


            TempData["course_list"] = obj_course.CourseList;


            CategoryDal obj_category = new CategoryDal();
            obj_category.categoryList = new SelectList(obj_category.category_List(), "category_id", "category_name");


            TempData["category_list"] = obj_category.categoryList;


            return View();

        }
        

        

        [NoDirectAccess]
      
        public ActionResult Edit(string id)
        {
         
            StudentDal sdb = new StudentDal();
            Course_Dal obj_dal = new Course_Dal();
            int courrseid = sdb.GetStudent_id(id).Course_Id;

            obj_dal.CourseList = new SelectList(obj_dal.Course_List(), "course_id", "course_name",
          obj_dal.Course_List().Find(smodel => smodel.course_id == courrseid).course_id);

            TempData["course_list"] = obj_dal.CourseList;


            CategoryDal obj_category = new CategoryDal();
            int cateogry_id = sdb.GetStudent_id(id).category_id;

           
            obj_category.categoryList = new SelectList(obj_category.category_List(), "category_id", "category_name",
            obj_category.category_List().Find(smodel => smodel.category_id == cateogry_id).category_id);

            TempData["category_list"] = obj_category.categoryList;


            return View(sdb.GetStudent_id(id));
       

        }
        // POST: Student/Edit/5

        [NoDirectAccess]
        [HttpPost]
        public ActionResult Edit(string str_id, StudentModel smodel)
        {
            try
            {
                StudentDal sdb = new StudentDal();
                if(sdb.UpdateStudent(smodel))
                {
                    TempData["message"] = "Updated Successfully";
                }
               
                return RedirectToAction("List");
            }
            catch(Exception e)
            {
                return View();
            }
        }

        [NoDirectAccess]
        public ActionResult Delete(string id)
        {
            try
            {
                StudentDal sdb = new StudentDal();
             
                if (sdb.deleteStudent(id))
                {
                    ViewBag.AlertMsg = " Deleted Successfully";
                    TempData["message"] = " Deleted Successfully";
                }
                return RedirectToAction("List");
            }
            catch
            {
                return View();
            }
        }



    }

  
    }