﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TKM_CENTRE_FOR_HIGHER_LEARNING.Models
{
    public class ParentModel
    {
        public ParentModel()
        {

        }
      
        public int parent_id { get; set; }
        [Display(Name = "First Name")]
        [RegularExpression(@"^[a-zA-Z_ ]+$", ErrorMessage = "Use letters only")]
        [Required(ErrorMessage = "First name  is required.")]
        public string first_name { get; set; }
        [Display(Name = "Last Name")]
        [RegularExpression(@"^[a-zA-Z_ ]+$", ErrorMessage = "Use letters only")]
        [Required(ErrorMessage = "Last name is required.")]
        public string last_name { get; set; }
        [Display(Name = "Address")]
        public string address { get; set; }
        public string City { get; set; }
        [Display(Name = "My Child")]
        public string student_id { get; set; }

        [Display(Name = "Phone Number")]
        [Required(ErrorMessage = "Phone number is required.")]
        [RegularExpression(@"^([0-9]{10,14})$", ErrorMessage = "Invalid Mobile Number.")]
        public string mobile_number { get; set; }
        [Display(Name = "E-Mail")]
        [RegularExpression(@"[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$", ErrorMessage = "Enter valid email")]
        [Required(ErrorMessage = "Email-id is required.")]
        public string email_id { get; set; }

        [Display(Name = "Gender")]
        public bool gender { get; set; }
        [Required(ErrorMessage = "User Name is required.")]
        [Display(Name = "User name")]
        public string user_name { get; set; }
        [Required(ErrorMessage = "Password is required.")]
        [Display(Name = "Password")]
        public string password { get; set; }
        [Display(Name = "Created By")]
        public string created_by { get; set; }
        public SelectList StudentNameList { get; set; }

        [Required(ErrorMessage = "* Required")]

        [DisplayName("Relationship")]
        public int id { get; set; }
       
        [Required(ErrorMessage = "* Required")]
        [DisplayName("Relationship")]
        public string Relationship { get; set; }
    }

    public class ParentDal
    {
        public bool AddParent(ParentModel pmodel,string student_id,int user_id)
        {
            dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
            dal_stored_procedure1.name = "[dbo].[AddNewParent]";
            dal_manager dal_manager1 = new dal_manager();
            dal_stored_procedure1.add_parameter("@first_name", pmodel.first_name, DbType.String, ParameterDirection.Input);
            dal_stored_procedure1.add_parameter("@last_name", pmodel.last_name, DbType.String, ParameterDirection.Input);
            dal_stored_procedure1.add_parameter("@address", pmodel.address, DbType.String, ParameterDirection.Input);
            dal_stored_procedure1.add_parameter("@gender", pmodel.gender, DbType.Boolean, ParameterDirection.Input);
            dal_stored_procedure1.add_parameter("@city", pmodel.City, DbType.String, ParameterDirection.Input);
            dal_stored_procedure1.add_parameter("@student_id", student_id, DbType.String, ParameterDirection.Input);
            dal_stored_procedure1.add_parameter("@mobile", pmodel.mobile_number, DbType.String, ParameterDirection.Input);
            dal_stored_procedure1.add_parameter("@email_id", pmodel.email_id, DbType.String, ParameterDirection.Input);
            //dal_stored_procedure1.add_parameter("@user_name", pmodel.user_name, DbType.String, ParameterDirection.Input);
            //dal_stored_procedure1.add_parameter("@password", pmodel.password, DbType.String, ParameterDirection.Input);
            dal_stored_procedure1.add_parameter("@created_by", user_id, DbType.Int32, ParameterDirection.Input);
            dal_stored_procedure1.add_parameter("@relationship_id", pmodel.id, DbType.Int32, ParameterDirection.Input);


            dal_manager1.execute_nonquery(dal_stored_procedure1);
            //int i= (int)dal_stored_procedure1.get_params()["@return_val"].param_value;

            return true;
        }
        public List<ParentModel> ParentList(int page_no)
        {
            List<ParentModel> list_parent = new List<ParentModel>();
            try
            {

                dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
                dal_stored_procedure1.name = "[dbo].[pr_get_Parent_list]";
                dal_manager dal_manager1 = new dal_manager();
                dal_stored_procedure1.add_parameter("@page_no", page_no, DbType.Int32, ParameterDirection.Input);
                DataSet ds_student = dal_manager1.execute_dataset(dal_stored_procedure1);
                foreach (DataRow row in ds_student.Tables[0].Rows)
                {
                    ParentModel obj = new ParentModel();
                    obj.parent_id =Convert.ToInt32( row["parent_id"]);
                    obj.first_name = row["name"].ToString();
                    obj.last_name = row["student_name"].ToString();
                    obj.gender =Convert.ToBoolean( row["gender"]);
                    obj.address = Convert.ToString(row["address"]);
                    obj.City = Convert.ToString(row["City"]);
                    //obj.user_name = Convert.ToString(row["user_name"]);
                    //obj.password = Convert.ToString(row["password"]);
                    obj.email_id = Convert.ToString(row["email_id"]);
                    obj.student_id = row["student_id"].ToString();
                    obj.mobile_number = row["mobile_number"].ToString();
                    obj.id = Convert.ToInt32(row["relationship_id"]);
                    obj.Relationship = Convert.ToString(row["Relationship"]);


                    list_parent.Add(obj);
                }
                return list_parent;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }

        }

        public List<ParentModel> Parent_by_student_id(string  student_id)
        {
            List<ParentModel> list_parent = new List<ParentModel>();
            try
            {

                dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
                dal_stored_procedure1.name = "[dbo].[pr_get_Parent_by_stude_id]";
                dal_manager dal_manager1 = new dal_manager();
                dal_stored_procedure1.add_parameter("@student_id", student_id, DbType.String, ParameterDirection.Input);
                DataSet ds_student = dal_manager1.execute_dataset(dal_stored_procedure1);
                foreach (DataRow row in ds_student.Tables[0].Rows)
                {
                    ParentModel obj = new ParentModel();
                    obj.parent_id = Convert.ToInt32(row["parent_id"]);
                    obj.first_name = row["first_name"].ToString();
                    obj.last_name = row["last_name"].ToString();
                    obj.gender = Convert.ToBoolean(row["gender"]);
                    obj.address = Convert.ToString(row["address"]);
                    obj.City = Convert.ToString(row["City"]);
                    //obj.user_name = Convert.ToString(row["user_name"]);
                    //obj.password = Convert.ToString(row["password"]);
                    obj.email_id = Convert.ToString(row["email_id"]);
                    obj.student_id = row["student_id"].ToString();
                    obj.mobile_number = row["mobile_number"].ToString();
                    obj.id = Convert.ToInt32(row["relationship_id"]);
                    obj.Relationship= row["Relationship"].ToString();


                    list_parent.Add(obj);
                }
                return list_parent;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }

        }

        public Boolean update_parent(ParentModel pmodel, string student_id)
        {
            try
            {

           

            dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
            dal_stored_procedure1.name = "[dbo].[UpdateParent]";
            dal_manager dal_manager1 = new dal_manager();
             dal_stored_procedure1.add_parameter("@parent_id", pmodel.parent_id, DbType.String, ParameterDirection.Input);
             dal_stored_procedure1.add_parameter("@first_name", pmodel.first_name, DbType.String, ParameterDirection.Input);
            dal_stored_procedure1.add_parameter("@last_name", pmodel.last_name, DbType.String, ParameterDirection.Input);
            dal_stored_procedure1.add_parameter("@address", pmodel.address, DbType.String, ParameterDirection.Input);
            dal_stored_procedure1.add_parameter("@gender", pmodel.gender, DbType.Boolean, ParameterDirection.Input);
            dal_stored_procedure1.add_parameter("@city", pmodel.City, DbType.String, ParameterDirection.Input);
            dal_stored_procedure1.add_parameter("@student_id", student_id, DbType.String, ParameterDirection.Input);
            dal_stored_procedure1.add_parameter("@relationship_id", pmodel.id, DbType.Int32, ParameterDirection.Input);
            dal_stored_procedure1.add_parameter("@mobile", pmodel.mobile_number, DbType.String, ParameterDirection.Input);
            dal_stored_procedure1.add_parameter("@email_id", pmodel.email_id, DbType.String, ParameterDirection.Input);
            //dal_stored_procedure1.add_parameter("@user_name", pmodel.user_name, DbType.String, ParameterDirection.Input);
            //dal_stored_procedure1.add_parameter("@password", pmodel.password, DbType.String, ParameterDirection.Input);
           


            dal_manager1.execute_nonquery(dal_stored_procedure1);
            //int i= (int)dal_stored_procedure1.get_params()["@return_val"].param_value;

            return true;
            }
            catch (Exception ex)
            {

                return false;
            }

        }
        public bool delete_parent(int id)
        {
            try
            {


                dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
                dal_stored_procedure1.name = "[dbo].[DeleteParent]";
                dal_manager dal_manager1 = new dal_manager();
                dal_stored_procedure1.add_parameter("@Parent_id", id, DbType.Int32, ParameterDirection.Input);

                dal_manager1.execute_nonquery(dal_stored_procedure1);
                //int i= (int)dal_stored_procedure1.get_params()["@return_val"].param_value;
            }
            catch (Exception e)
            {

                return false;
            }
            return true;
        }


        public ParentModel Parent_by_studentid(string student_id)
        {

            try
            {
                ParentModel obj = new ParentModel();

                dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
                dal_stored_procedure1.name = "[dbo].[pr_get_Parent_by_student_id]";
                dal_stored_procedure1.add_parameter("@student_id", student_id, DbType.String, ParameterDirection.Input);
                dal_manager dal_manager1 = new dal_manager();
                DataSet ds_staff = dal_manager1.execute_dataset(dal_stored_procedure1);
                foreach (DataRow row in ds_staff.Tables[0].Rows)
                {
                    obj.parent_id = Convert.ToInt32(row["parent_id"]);
                    obj.first_name = row["first_name"].ToString();
                    obj.last_name = row["last_name"].ToString();
                    obj.gender = Convert.ToBoolean(row["gender"]);
                    obj.address = Convert.ToString(row["address"]);
                    obj.City = Convert.ToString(row["City"]);
                    //obj.user_name = Convert.ToString(row["user_name"]);
                    //obj.password = Convert.ToString(row["password"]);
                    obj.email_id = Convert.ToString(row["email_id"]);
                    obj.student_id = row["student_id"].ToString();
                    obj.mobile_number = row["mobile_number"].ToString();
                    obj.id = Convert.ToInt32(row["relationship_id"]);
                    obj.Relationship = Convert.ToString(row["_name"]);

                }

                return obj;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }

        }

    }
}

   
    
  