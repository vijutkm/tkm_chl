﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TKM_CENTRE_FOR_HIGHER_LEARNING.Models
{
    public class Teachingmodel
    {


        [Display(Name = "Id")]
        public int staff_id { get; set; }

        [Display(Name = "First Name")]
        [RegularExpression(@"^[a-zA-Z_ ]+$", ErrorMessage = "Use letters only")]
        [Required(ErrorMessage = "First name is required.")]
        public string first_name { get; set; }

        [Display(Name = "Last Name")]
        [RegularExpression(@"^[a-zA-Z_ ]+$", ErrorMessage = "Use letters only")]
        [Required(ErrorMessage = "lastname is required.")]
        public string last_name { get; set; }
        

        [Display(Name = "Date of Birth")]
        [Required(ErrorMessage = "Date of Birth is required.")]
        public DateTime DOB { get; set; }

        //[Display(Name = "Date of Birth")]
        //[Required(ErrorMessage = "Date of Birth is required.")]
        //public string DOBedit { get; set; }

        [Display(Name = "Gender")]
        [Required(ErrorMessage = "Gender is required.")]
        public int gender { get; set; }

        private SqlConnection con;
        private void connection()
        {
            string constring = ConfigurationManager.AppSettings["conStr"].ToString();
            con = new SqlConnection(constring);
        }
        [Required(ErrorMessage = "Username is required.")]
        [RegularExpression(@"^[a-zA-Z0-9]+$", ErrorMessage = "only letters and numbers")]
        [Display(Name = "Username")]
        public string username { get; set; }

        [Required(ErrorMessage = "Password is required.")]
        [Display(Name = "Password")]
        public string password { get; set; }

        [Display(Name = "E-Mail")]
        [Required(ErrorMessage = "E-Mail is required.")]
        [RegularExpression(@"[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$", ErrorMessage = "Enter valid email")]
        public string email_id { get; set; }

        [Display(Name = "Phone Number")]
        [RegularExpression(@"^([0-9]{10,14})$", ErrorMessage = "Invalid Mobile Number.")]
        [Required(ErrorMessage = "Phone Number is required.")]
        public string phone_number { get; set; }


        [Display(Name = "Department Name")]
        [Required(ErrorMessage = "Department is required.")]
        public int department_id { get; set; }


        [Display(Name="Course")]
        public string department_name { get; set; }


        [Display(Name = "Role Name")]
        [Required(ErrorMessage = "Role is required.")]
        public int role_id { get; set; }

        [Display(Name = "Role")]
        public string role_name { get; set; }

        [Display(Name = "Title")]
        public int title_id { get; set; }

        [Display(Name = "Staff Number")]        
        public string staff_no { get; set; }

        [Display(Name = "PEN")]        
        public string pen { get; set; }

        [Display(Name = "Appointment Type")]
        public string appointment_type { get; set; }

        public int AddStaff(Teachingmodel smodel)
        {

            int i = -1;
            try
            {

                dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
                dal_stored_procedure1.name = "[dbo].[AddNewstaff]";
                dal_manager dal_manager1 = new dal_manager();
                dal_stored_procedure1.add_parameter("@first_name", smodel.first_name, DbType.String, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@last_name", smodel.last_name, DbType.String, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@department_id", smodel.department_id, DbType.Int32, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@DOB", smodel.DOB, DbType.Date, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@gender", smodel.gender, DbType.Int32, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@username", smodel.username, DbType.String, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@password", smodel.password, DbType.String, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@email", smodel.email_id, DbType.String,ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@mobile", smodel.phone_number, DbType.String, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@role_id", smodel.role_id, DbType.Int32, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@title_id", smodel.title_id, DbType.Int32, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@staff_no", smodel.staff_no, DbType.String, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@pen", smodel.pen, DbType.String, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@appointment_type", smodel.appointment_type, DbType.String, ParameterDirection.Input);

                dal_stored_procedure1.add_parameter("@return_value", null, DbType.Int32, ParameterDirection.Output);
                dal_manager1.execute_nonquery(dal_stored_procedure1);
                i = (int)dal_stored_procedure1.get_params()["@return_value"].param_value;

            }
            catch (Exception ex)
            {
                i = -1;
                return i;
            }
            finally
            {


            }
            return i;

            //connection();
            //SqlCommand cmd = new SqlCommand("AddNewstaff", con);
            //cmd.CommandType = CommandType.StoredProcedure;

            //cmd.Parameters.AddWithValue("@first_name", smodel.first_name);
            //cmd.Parameters.AddWithValue("@last_name", smodel.last_name);
            //cmd.Parameters.AddWithValue("@department_id", smodel.department_id);
            //cmd.Parameters.AddWithValue("@DOB", smodel.DOB);
            //cmd.Parameters.AddWithValue("@gender", smodel.gender);
            //cmd.Parameters.AddWithValue("@username", smodel.username);
            //cmd.Parameters.AddWithValue("@password", smodel.password);
            //cmd.Parameters.AddWithValue("@email", smodel.email_id);
            //cmd.Parameters.AddWithValue("@mobile", smodel.phone_number);
            //cmd.Parameters.AddWithValue("@role_id", smodel.role_id);
            //con.Open();
            //int i = cmd.ExecuteNonQuery();
            //con.Close();

            //if (i >= 1)
            //    return true;
            //else
            //    return false;
        }


        public bool DeleteStaff(int id)
        {
            connection();
            SqlCommand cmd = new SqlCommand("Deletestaff", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@staff_id", id);

            con.Open();
            int i = cmd.ExecuteNonQuery();
            con.Close();

            if (i >= 1)
                return true;
            else
                return false;
        }




        public bool UpdateDetails(Teachingmodel smodel)
        {
            connection();
            SqlCommand cmd = new SqlCommand("UpdateStaffDetails", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@staff_id", smodel.staff_id);
            cmd.Parameters.AddWithValue("@first_name", smodel.first_name);
            cmd.Parameters.AddWithValue("@last_name", smodel.last_name);
            cmd.Parameters.AddWithValue("@department_id", smodel.department_id);
            cmd.Parameters.AddWithValue("@DOB", smodel.DOB);
            cmd.Parameters.AddWithValue("@gender", smodel.gender);
            cmd.Parameters.AddWithValue("@username", smodel.username);
            //cmd.Parameters.AddWithValue("@password", smodel.password);
            cmd.Parameters.AddWithValue("@email", smodel.email_id);
            cmd.Parameters.AddWithValue("@mobile_number", smodel.phone_number);
            cmd.Parameters.AddWithValue("@role_id", smodel.role_id);
            con.Open();
            int i = cmd.ExecuteNonQuery();
            con.Close();

            if (i >= 1)
                return true;
            else
                return false;
        }


    }

    public class StaffDal
    {
        
        public List<Teachingmodel> GetStaff()
        {
           
            List<Teachingmodel> list_staff_model = new List<Teachingmodel>();
            try
            {

                dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
                dal_stored_procedure1.name = "[dbo].[GetStaffDetails]";
                dal_manager dal_manager1 = new dal_manager();
                DataSet ds_staff = dal_manager1.execute_dataset(dal_stored_procedure1);
                foreach (DataRow row in ds_staff.Tables[0].Rows)
                {
                    Teachingmodel staff_model = new Teachingmodel();
                    staff_model.staff_id = Convert.ToInt32(row["staff_id"]);
                    staff_model.first_name = row["first_name"].ToString();
                    staff_model.department_name = row["department_name"].ToString();
                    staff_model.DOB = Convert.ToDateTime(row["dob"]);
                    staff_model.gender = Convert.ToInt32(row["gender"]);
                    staff_model.phone_number= row["mobile_number"].ToString();
                    staff_model.email_id = row["email"].ToString();
                    staff_model.role_name= row["role_name"].ToString();
                    list_staff_model.Add(staff_model);
                }
                return list_staff_model;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }

        }
        public Teachingmodel GetStaff(int staff_id)
        {
           
            try
            {
                Teachingmodel staff_model = new Teachingmodel();

                dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
                dal_stored_procedure1.name = "[dbo].[pr_get_staff_details_by_staff_id]";
                dal_stored_procedure1.add_parameter("@staff_id", staff_id, DbType.String, ParameterDirection.Input);
                dal_manager dal_manager1 = new dal_manager();
                DataSet ds_staff = dal_manager1.execute_dataset(dal_stored_procedure1);
                foreach (DataRow row in ds_staff.Tables[0].Rows)
                {
                   
                    staff_model.staff_id = Convert.ToInt32(row["staff_id"]);
                    staff_model.first_name = row["first_name"].ToString();
                    staff_model.last_name = row["last_name"].ToString();
                    staff_model.department_id =Convert.ToInt32( row["department_id"]);
                    staff_model.DOB = Convert.ToDateTime(row["dob"]);
                    staff_model.gender = Convert.ToInt32(row["gender"]);
                    staff_model.username= row["username"].ToString();
                    staff_model.phone_number = row["mobile_number"].ToString();

                    staff_model.email_id = row["email"].ToString();
                    staff_model.role_id= Convert.ToInt32(row["role_id"]);

                }
                return staff_model;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }

        }
        public List<Teachingmodel> GetStaff_by_role(int id)
        {

            List<Teachingmodel> list_staff_model = new List<Teachingmodel>();
            try
            {

                dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
                dal_stored_procedure1.name = "[dbo].[pr_get_staff_by_role]";
                dal_stored_procedure1.add_parameter("@role", id, DbType.String, ParameterDirection.Input);
                dal_manager dal_manager1 = new dal_manager();
                DataSet ds_staff = dal_manager1.execute_dataset(dal_stored_procedure1);
                foreach (DataRow row in ds_staff.Tables[0].Rows)
                {
                    Teachingmodel staff_model = new Teachingmodel();
                    staff_model.staff_id = Convert.ToInt32(row["staff_id"]);
                    staff_model.first_name = row["first_name"].ToString();                   
                    list_staff_model.Add(staff_model);
                }
                return list_staff_model;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }

        }

    }

}