﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web;

namespace TKM_CENTRE_FOR_HIGHER_LEARNING.Models
{
    public class ReportStudentFee
    {
        [DisplayName("Course")]
        public int Course_Id { get; set; }
        [Required(ErrorMessage = "* Required")]
        [DisplayName("Course")]
        public string Course_name { get; set; }

        [Required(ErrorMessage = "* Required")]
        [Range(1, 6, ErrorMessage = "Please use values between 1 to 6")]
        [DisplayName("Semester")]
        public int Semester { get; set; }

        [Display(Name = "From Date")]
      //  [Compare("to_date", ErrorMessage = "From date cannot be greater than to date")]
        [Required(ErrorMessage = "From date is required.")]
        public DateTime from_date { get; set; }

        [Display(Name = "To Date")]
        [Required(ErrorMessage = "To date is required.")]
        public DateTime to_date { get; set; }
    }
    //public class  FeeReportDal
    //{
    //    public List<ReportStudentFee> FeeList(ReportStudentFee obj)
    //    {
    //        List<ReportStudentFee> list_student_model = new List<ReportStudentFee>();
    //        try
    //        {

    //            dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
    //            dal_stored_procedure1.name = "[dbo].[get_student_fee]";
    //            dal_manager dal_manager1 = new dal_manager();
    //            dal_stored_procedure1.add_parameter("@course_id", obj.Course_Id, DbType.Int32, ParameterDirection.Input);
    //            dal_stored_procedure1.add_parameter("@sem", obj.Semester, DbType.Int32, ParameterDirection.Input);
    //            dal_stored_procedure1.add_parameter("@fromdate", obj.from_date, DbType.Date, ParameterDirection.Input);
    //            dal_stored_procedure1.add_parameter("@todate", obj.from_date, DbType.Date, ParameterDirection.Input);
    //            DataSet ds_student_fee = dal_manager1.execute_dataset(dal_stored_procedure1);
    //            foreach (DataRow row in ds_student_fee.Tables[0].Rows)
    //            {
    //                //ReportStudentFee fee_model = new ReportStudentFee();
    //                //fee_model

    //                //student_model.strStudentId = row["StudentName"].ToString();
    //                //student_model.strFirstName = row["Course"].ToString();
    //                //student_model.strLastName = row["Sem"].ToString();
    //                //student_model.strMobileNumber = row["TotalFee"].ToString();
    //                //student_model.dtDOB = (DateTime)row["PaidFee"];
    //                //student_model.Course_name = row["Fine"].ToString();
    //                //student_model.category_name = row["PaymentDate"].ToString();
    //                //student_model.Semester = Convert.ToInt32(row["semester"]);
    //                //student_model.boolGender = Convert.ToBoolean(row["gender"]);

    //                //list_student_model.Add(student_model);
    //            }
    //            return list_student_model;

    //        }
    //        catch (Exception ex)
    //        {
    //            throw ex;
    //        }
    //        finally
    //        {
    //        }

    //    }
    //}
}