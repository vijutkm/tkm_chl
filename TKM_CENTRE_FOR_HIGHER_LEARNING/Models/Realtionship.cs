﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TKM_CENTRE_FOR_HIGHER_LEARNING.Models
{
    public class Realtionship
    {
        [Display(Name = "Relationship")]
        public int id { get; set; }
        [Display(Name = "Relationship")]
        public string Relationship { get; set; }
    }




    public class Relationship_Dal
    {
        public SelectList RelationshipList { get; set; }
        public List<Realtionship> Realtionship_List()
        {
            List<Realtionship> list_Realtionship = new List<Realtionship>();
            try
            {

                dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
                dal_stored_procedure1.name = "[dbo].[pr_get_relationship_list]";
                dal_manager dal_manager1 = new dal_manager();
                DataSet ds_relationship = dal_manager1.execute_dataset(dal_stored_procedure1);
                foreach (DataRow row in ds_relationship.Tables[0].Rows)
                {
                    Realtionship realtionship = new Realtionship();
                    realtionship.id = Convert.ToInt32(row["id"]);
                    realtionship.Relationship = row["Relationship"].ToString();


                    list_Realtionship.Add(realtionship);
                }
                return list_Realtionship;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }

        }
    }
}