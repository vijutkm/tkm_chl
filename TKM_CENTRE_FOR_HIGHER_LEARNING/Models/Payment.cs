﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web;

namespace TKM_CENTRE_FOR_HIGHER_LEARNING.Models
{
    public class Payment
    {
        public string transaction_id { get; set; }
        [DisplayName("Payment Type")]
        [Required(ErrorMessage = "* Required")]
        public bool type { get; set; }
        [DisplayName("Payment Category")]
        public int payment_category_id { get; set; }
        [DisplayName("Description")]
        public string description { get; set; }
        [DisplayName("Payer")]
        [Required(ErrorMessage = "* Required")]
        public string payer { get; set; }
        [DisplayName("Payment Date")]
        [Required(ErrorMessage = "* Required")]
        public DateTime date { get; set; }
        public int created_by { get; set; }
        public DateTime created_on { get; set; }
        [DisplayName("Payment Amount")]
        [Required(ErrorMessage = "* Required")]
        public decimal pay_amount { get; set; }
        public Payment_Category payment_Category = new Payment_Category();
    }

    public class PaymentDAL
    {


        public bool save_payment( Payment pay)
        {
            try
            {

           
            dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
            dal_stored_procedure1.name = "[dbo].[pr_add_income_expenses]";
            dal_manager dal_manager1 = new dal_manager();
            dal_stored_procedure1.add_parameter("@payment_category_id", pay.payment_category_id, DbType.Int32, ParameterDirection.Input);
            dal_stored_procedure1.add_parameter("@type", pay.type, DbType.Boolean, ParameterDirection.Input);
            dal_stored_procedure1.add_parameter("@payer", pay.payer, DbType.String, ParameterDirection.Input);
            dal_stored_procedure1.add_parameter("@pay_amount", pay.pay_amount, DbType.Decimal, ParameterDirection.Input);
            dal_stored_procedure1.add_parameter("@date", pay.date, DbType.Date, ParameterDirection.Input);
            dal_stored_procedure1.add_parameter("@description", pay.description, DbType.String, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@created_by", 1, DbType.Int32, ParameterDirection.Input);


                dal_manager1.execute_nonquery(dal_stored_procedure1);
                //int i= (int)dal_stored_procedure1.get_params()["@return_val"].param_value;

            }
            catch (Exception ex)
            {

                throw;
            }

            return true;
        }
        public List<Payment> Get_all_payment_List()
        {

            List<Payment> list_Payment = new List<Payment>();
            try
            {

                dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
                dal_stored_procedure1.name = "[dbo].[pr_get_all_payment_list]";
                dal_manager dal_manager1 = new dal_manager();
                DataSet ds_staff = dal_manager1.execute_dataset(dal_stored_procedure1);

                foreach (DataRow row in ds_staff.Tables[0].Rows)
                {
                    Payment _payment = new Payment();
                    _payment.transaction_id = Convert.ToString(row["transaction_id"]);
                    _payment.payment_Category.category_name = Convert.ToString(row["category_name"]);
                    _payment.description = Convert.ToString(row["description"]);
                    _payment.payer = Convert.ToString(row["payer"]);
                    _payment.date = Convert.ToDateTime(row["date"]);
                    _payment.type = Convert.ToBoolean(row["type"]);
                    _payment.pay_amount= Convert.ToDecimal(row["pay_amount"]);
                    list_Payment.Add(_payment);
                }
                return list_Payment;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }

        }
        public DataSet get_payment_details()
        {
            try
            {

                dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
                dal_stored_procedure1.name = "[dbo].[pr_get_all_payment_summary]";  
                dal_manager dal_manager1 = new dal_manager();
                DataSet ds_student = dal_manager1.execute_dataset(dal_stored_procedure1);
                return ds_student;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }

        }
    }
}