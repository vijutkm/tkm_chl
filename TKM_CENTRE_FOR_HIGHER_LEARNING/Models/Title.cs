﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TKM_CENTRE_FOR_HIGHER_LEARNING.Models
{
    public class Title
    {
        public int title_id { get; set; }
        public string title_name { get; set; }
    }
    public class TitleDal
    {
        public SelectList TitleList { get; set; }
        public List<Title> Title_List()
        {
            List<Title> list_Title = new List<Title>();
            try
            {

                dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
                dal_stored_procedure1.name = "[dbo].[pr_get_title_list]";
                dal_manager dal_manager1 = new dal_manager();
                DataSet ds_student = dal_manager1.execute_dataset(dal_stored_procedure1);
                foreach (DataRow row in ds_student.Tables[0].Rows)
                {
                    Title title = new Title();
                    title.title_id = Convert.ToInt32(row["title_id"]);
                    title.title_name = row["title_name"].ToString();
                    list_Title.Add(title);
                }
                return list_Title;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }

        }
    }
}