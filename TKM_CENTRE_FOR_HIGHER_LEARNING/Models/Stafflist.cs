﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TKM_CENTRE_FOR_HIGHER_LEARNING.Models
{
    public class Stafflist
    {


        public int staff_id { get; set; }
      
        public string staff_name { get; set; }
    }


    public class staff_Dal
    {
        public SelectList staffList { get; set; }
        public List<Stafflist> staff_List()
        {
            List<Stafflist> list_staff = new List<Stafflist>();
            try
            {

                dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
                dal_stored_procedure1.name = "[dbo].[pr_get_staff_list]";
                dal_manager dal_manager1 = new dal_manager();
                DataSet ds_staff = dal_manager1.execute_dataset(dal_stored_procedure1);

                Stafflist staff1 = new Stafflist();
                staff1.staff_id = 0;
                staff1.staff_name = "-- All --";
                list_staff.Add(staff1);
                foreach (DataRow row in ds_staff.Tables[0].Rows)
                {
                    Stafflist staff = new Stafflist();
                    staff.staff_id = Convert.ToInt32(row["staff_id"]);
                    staff.staff_name = row["name"].ToString();
                   

                    list_staff.Add(staff);
                }

              
                return list_staff;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }

        }
    }
}