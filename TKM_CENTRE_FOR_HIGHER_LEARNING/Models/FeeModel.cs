﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web;

namespace TKM_CENTRE_FOR_HIGHER_LEARNING.Models
{
    public class FeeModel
    {
        public int fee_id { get; set; }
        public int fee_settings_id { get; set; }
        public string student_id { get; set; }
        public decimal amount { get; set; }
        public int created_by { get; set; }
        public decimal fine_amount { get; set; }
        public int is_due { get; set; }
        public DateTime created_on { get; set; }
        public FeeType feeType = new FeeType();

        public Course course = new Course();

        public StudentModel StudentModel = new StudentModel();

    }

    public class collection_fee
    {
        //public DateTime date { get; set; }
        //public int pay_amount { get; set; }



        public DataSet collection_Fee_details_for_chart()
        {
            try
            {

                dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
                dal_stored_procedure1.name = "[dbo].[pr_get_collection_fee_payment_details]";
                // dal_stored_procedure1.add_parameter("@student_id", ID, DbType.String, ParameterDirection.Input);

                dal_manager dal_manager1 = new dal_manager();
                DataSet ds_collection = dal_manager1.execute_dataset(dal_stored_procedure1);


                return ds_collection;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }

        }
    }


    

    public class FeeDal
    {
        public List<FeeModel> Get_all_fee_List(string studentid)
        {

            List<FeeModel> list_FeeModel = new List<FeeModel>();
            try
            {

                dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
                dal_stored_procedure1.name = "[dbo].[pr_get_all_fee_paied_list]";
                dal_manager dal_manager1 = new dal_manager();
                dal_stored_procedure1.add_parameter("@student_id", studentid, DbType.String, ParameterDirection.Input);
                DataSet ds_staff = dal_manager1.execute_dataset(dal_stored_procedure1);
           
                foreach (DataRow row in ds_staff.Tables[0].Rows)
                {
                    FeeModel _feeModel = new FeeModel();
                    _feeModel.fee_id = Convert.ToInt32(row["fee_id"]);
                    _feeModel.StudentModel.strStudentId = Convert.ToString(row["student_id"]);
                    _feeModel.StudentModel.strFirstName = Convert.ToString(row["first_name"]);
                    _feeModel.feeType.fee_type= Convert.ToString(row["fee_type"]);
                    _feeModel.amount = Convert.ToInt32(row["amount"]);
                    _feeModel.created_on=Convert.ToDateTime(row["created_on"]);
                    _feeModel.course.course_name= Convert.ToString(row["course_name"]);
                    _feeModel.StudentModel.Semester= Convert.ToInt32(row["Semester"]);
                   // _feeModel.fee_settings_id = Convert.ToInt32(row["fee_settings_id"]);
                   

                    list_FeeModel.Add(_feeModel);
                }
                return list_FeeModel;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }

        }

        public bool AddFee(FeeModel model)
        {
            try
            {

            
            dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
            dal_stored_procedure1.name = "[dbo].[pr_add_fee_by_student]";
            dal_manager dal_manager1 = new dal_manager();
            dal_stored_procedure1.add_parameter("@amount", model.amount, DbType.Decimal, ParameterDirection.Input);
            dal_stored_procedure1.add_parameter("@created_by", model.created_by, DbType.Int32, ParameterDirection.Input);
            dal_stored_procedure1.add_parameter("@fee_settings_id", model.fee_settings_id, DbType.Int32, ParameterDirection.Input);
            dal_stored_procedure1.add_parameter("@student_id", model.student_id, DbType.String, ParameterDirection.Input);
            dal_stored_procedure1.add_parameter("@fine_amount", model.fine_amount, DbType.Decimal, ParameterDirection.Input);
            dal_stored_procedure1.add_parameter("@is_due", model.is_due, DbType.Int32, ParameterDirection.Input);

                dal_manager1.execute_nonquery(dal_stored_procedure1);
                //int i= (int)dal_stored_procedure1.get_params()["@return_val"].param_value;
            }
            catch (Exception ex)
            {

                throw;
            }
            return true;
        }

        public DataSet get_student_Fee_status(string ID)
        {
            try
            {

                dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
                dal_stored_procedure1.name = "[dbo].[pr_get_student_fee_payment_status_by_id]";
                dal_stored_procedure1.add_parameter("@student_id", ID, DbType.String, ParameterDirection.Input);

                dal_manager dal_manager1 = new dal_manager();
                DataSet ds_student = dal_manager1.execute_dataset(dal_stored_procedure1);

                
                return ds_student;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }

        }


      
    }
}