﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web;

namespace TKM_CENTRE_FOR_HIGHER_LEARNING.Models
{
    public class Leave
    {
        public int leave_id { get; set; }
        [Display(Name = "Leave Type")]
        public int leave_type_id { get; set; }
        public int staff_id { get; set; }
        [Required]
        [Display(Name = "From Date")]
        public DateTime start_date { get; set; }
        [Required]
        [Display(Name = "To Date")]
        public DateTime end_date { get; set; }
        [Display(Name = "Total Leave")]
        public decimal number_of_leaves { get; set; }
        [Display(Name = "Leave Status")]
        public string leave_status { get; set; }
        [Display(Name = "Assign To")]
        public int assigned_to { get; set; }
        [Required]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Leave Reason")]
        public string reason { get; set; }
        [Display(Name = "Emergency Contact Phone")]
        [Required]
        public string emergancy_contact_number { get; set; }

        public LeaveType leaveType { get; set; }
        public string remarks { get; set; }
    }

    public class LeaveDal
    {
        public int save_leave(Leave model)
        {
            int i = 0;
            try
            {


                dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
                dal_stored_procedure1.name = "[dbo].[pr_add_leave]";
                dal_manager dal_manager1 = new dal_manager();
                dal_stored_procedure1.add_parameter("@leave_type_id", model.leave_type_id, DbType.Int32, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@number_of_leaves", model.number_of_leaves, DbType.Decimal, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@reason", model.reason, DbType.String, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@assigned_to", model.assigned_to, DbType.Int32, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@staff_id", model.staff_id, DbType.Int32, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@start_date", model.start_date, DbType.DateTime, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@end_date", model.end_date, DbType.DateTime, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@leave_id", model.leave_id, DbType.Int32, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@emergancy_contact_number", model.emergancy_contact_number, DbType.String, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@return_val", null, DbType.Int32, ParameterDirection.Output);
                dal_manager1.execute_nonquery(dal_stored_procedure1);
                 i= (int)dal_stored_procedure1.get_params()["@return_val"].param_value;
            }
            catch (Exception ex)
            {

                throw;
            }
            return i;
        }
        public int update_leave(Leave model)
        {
            int i = 0;
            try
            {


                dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
                dal_stored_procedure1.name = "[dbo].[pr_add_leave_histoty]";
                dal_manager dal_manager1 = new dal_manager();
                dal_stored_procedure1.add_parameter("@leave_id", model.leave_id, DbType.Int32, ParameterDirection.Input);               
                dal_stored_procedure1.add_parameter("@assigned_to", model.assigned_to, DbType.Int32, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@staff_id", model.staff_id, DbType.Int32, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@leave_status", model.leave_status, DbType.String, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@remarks", model.remarks, DbType.String, ParameterDirection.Input);
                
                dal_stored_procedure1.add_parameter("@return_val", null, DbType.Int32, ParameterDirection.Output);
                dal_manager1.execute_nonquery(dal_stored_procedure1);
                i = (int)dal_stored_procedure1.get_params()["@return_val"].param_value;
            }
            catch (Exception ex)
            {

                throw;
            }
            return i;
        }
        public DataSet Get_Staff_leave_List(int ID)
        {

           // List<Leave> list_LeaveModel = new List<Leave>();
            try
            {

                dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
                dal_stored_procedure1.name = "[dbo].[pr_get_leave_by_staff_id]";
                dal_stored_procedure1.add_parameter("@staff_ID", ID, DbType.Int32, ParameterDirection.Input);

                dal_manager dal_manager1 = new dal_manager();
                DataSet ds_staff = dal_manager1.execute_dataset(dal_stored_procedure1);

               /* foreach (DataRow row in ds_staff.Tables[0].Rows)
                {
                    Leave _Leave = new Leave();
                    _Leave.leave_id = Convert.ToInt32(row["leave_id"]);
                    _Leave.leaveType.leave_type = Convert.ToString(row["leave_type"]);
                    _Leave.leave_status = Convert.ToString(row["leave_status"]);                   
                    _Leave.number_of_leaves =Convert.ToDecimal( row["number_of_leaves"].ToString());

                    list_LeaveModel.Add(_Leave);
                }*/
                return ds_staff;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }

        }
        public DataSet Get_Assigned_leave_List(int ID)
        {

            // List<Leave> list_LeaveModel = new List<Leave>();
            try
            {

                dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
                dal_stored_procedure1.name = "[dbo].[pr_get_leave_assigned_by_staff_id]";
                dal_stored_procedure1.add_parameter("@staff_ID", ID, DbType.Int32, ParameterDirection.Input);

                dal_manager dal_manager1 = new dal_manager();
                DataSet ds_staff = dal_manager1.execute_dataset(dal_stored_procedure1);

                /* foreach (DataRow row in ds_staff.Tables[0].Rows)
                 {
                     Leave _Leave = new Leave();
                     _Leave.leave_id = Convert.ToInt32(row["leave_id"]);
                     _Leave.leaveType.leave_type = Convert.ToString(row["leave_type"]);
                     _Leave.leave_status = Convert.ToString(row["leave_status"]);                   
                     _Leave.number_of_leaves =Convert.ToDecimal( row["number_of_leaves"].ToString());

                     list_LeaveModel.Add(_Leave);
                 }*/
                return ds_staff;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }

        }
        public DataSet Get_Staff_leave_status(int ID)
        {
                        
            try
            {

                dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
                dal_stored_procedure1.name = "[dbo].[pr_get_leave_status_staff_id]";
                dal_stored_procedure1.add_parameter("@staff_ID", ID, DbType.Int32, ParameterDirection.Input);

                dal_manager dal_manager1 = new dal_manager();
                DataSet ds_staff = dal_manager1.execute_dataset(dal_stored_procedure1);
               
                return ds_staff;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }

        }
    }
}