﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TKM_CENTRE_FOR_HIGHER_LEARNING.Models
{
    public class Student
    {
      
        public int student_id { get; set; }


        [Required(ErrorMessage = "Address is required.")]
        public string Address { get; set; }
        [Required(ErrorMessage = "City is required.")]
        public string first_name { get; set; }
        [Required(ErrorMessage = "City is required.")]
        public string last_name { get; set; }
        public DateTime dob { get; set; }

        public Boolean gender { get; set; }
        [Required(ErrorMessage = "City is required.")]
        public string mobile_number { get; set; }
        public int course_id { get; set; }
        public int semester { get; set; }
        public int status_id { get; set; }
        public int Course_Id { get; set; }
        public int category_id { get; set; }
    }
}
