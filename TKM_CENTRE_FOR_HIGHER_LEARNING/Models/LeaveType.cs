﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TKM_CENTRE_FOR_HIGHER_LEARNING.Models
{
    public class LeaveType
    {
        [Display(Name = "Leave Type")]
        public int leave_type_id { get; set; }
        public string leave_type { get; set; }
        public int leave_total_nuumber { get; set; }
        public int status { get; set; }
    }
    public class LeaveTypeDal
    {
        public SelectList select_list_leave_type { get; set; }
        public List<LeaveType> leave_type_List()
        {
            List<LeaveType> list_leave_type = new List<LeaveType>();
            try
            {

                dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
                dal_stored_procedure1.name = "[dbo].[pr_get_leave_type]";
                dal_manager dal_manager1 = new dal_manager();
                DataSet ds_student = dal_manager1.execute_dataset(dal_stored_procedure1);
                foreach (DataRow row in ds_student.Tables[0].Rows)
                {
                    LeaveType leaveType = new LeaveType();
                    leaveType.leave_type_id = Convert.ToInt32(row["leave_type_id"]);
                    leaveType.leave_type = row["leave_type"].ToString();


                    list_leave_type.Add(leaveType);
                }
                return list_leave_type;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }

        }
        
    }
}