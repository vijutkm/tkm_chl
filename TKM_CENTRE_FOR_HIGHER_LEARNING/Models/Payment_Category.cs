﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace TKM_CENTRE_FOR_HIGHER_LEARNING.Models
{
    public class Payment_Category
    {
        public int payment_category_id { get; set; }
        public string category_name { get; set; }
        public string description { get; set; }
        public int status { get; set; }
    }
    public class Payment_Category_DAL
    {
        public List<Payment_Category> payment_Category_list()
        {
            List<Payment_Category> payment_Category = new List<Payment_Category>();
            try
            {

                dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
                dal_stored_procedure1.name = "[dbo].[pr_get_payment_category_list]";
                dal_manager dal_manager1 = new dal_manager();              
                DataSet ds_student = dal_manager1.execute_dataset(dal_stored_procedure1);
                foreach (DataRow row in ds_student.Tables[0].Rows)
                {
                    Payment_Category _payment_Category = new Payment_Category();
                    _payment_Category.payment_category_id =Convert.ToInt32( row["payment_category_id"]);
                    _payment_Category.category_name = row["category_name"].ToString(); 
                    payment_Category.Add(_payment_Category);
                }
                return payment_Category;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }

        }
    }
}