﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web;

namespace TKM_CENTRE_FOR_HIGHER_LEARNING.Models
{
    public class FeeSettings
    {
        public int fee_settings_id { get; set; }       
        [Required(ErrorMessage = "Semester is required.")]      
        public int semester { get; set; }        
        [Required(ErrorMessage = "Fee amount is required.")]
        [Display(Name = "Fee Amount")]
        public decimal fee { get; set; }
        public int created_by { get; set; }
        public decimal balance { get; set; }
        //[DataObjectField(false)]
        //[DataType(DataType.Date)]
        [Display(Name = "Due Date")]
        [Required(ErrorMessage = "Due date is required.")]
        public DateTime due_date { get; set; }

        public DateTime last_payment_date { get; set; }
        public decimal last_payment { get; set; }

        [Display(Name = "Fine Amount")]
        public decimal fine_amount { get; set; }

       
        public int is_due { get; set; }

        public Course course = new Course();
        public FeeType feetype = new FeeType();
        public StudentCategory studentCategory = new StudentCategory();

        public int course_id { get; set; }
        public int fee_type_id { get; set; }
        public int category_id { get; set; }
       
       

    }
    public class FeeSettingsDal
    {
        public List<FeeSettings> Get_FeeSettings_List( string ID)
        {

            List<FeeSettings> list_FeeModel = new List<FeeSettings>();
            try
            {

                dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
                dal_stored_procedure1.name = "[dbo].[pr_get_fee_settings_list_by_student_id]";
                dal_stored_procedure1.add_parameter("@student_id", ID, DbType.String, ParameterDirection.Input);
               
                dal_manager dal_manager1 = new dal_manager();
                DataSet ds_staff = dal_manager1.execute_dataset(dal_stored_procedure1);

                foreach (DataRow row in ds_staff.Tables[0].Rows)
                {
                    FeeSettings _feeModel = new FeeSettings();
                    _feeModel.fee_settings_id = Convert.ToInt32(row["fee_settings_id"]);
                    _feeModel.fee = Convert.ToDecimal(row["fee"]);
                    _feeModel.semester = Convert.ToInt32(row["semester"]);
                    //_feeModel.course_id = Convert.ToInt32(row["course_id"]);
                    _feeModel.feetype.fee_type= row["fee_type"].ToString();
                   _feeModel.balance = Convert.ToInt32(row["balance"]);
                   // _feeModel.balance = Convert.ToDecimal(row["fee"]) - Convert.ToDecimal(row["amount_paid"]);
                    _feeModel.due_date= Convert.ToDateTime(row["due_date"]);
                    _feeModel.fine_amount= Convert.ToDecimal(row["fine_amount"]);
                    _feeModel.is_due = Convert.ToInt32(row["is_due"]);
                    list_FeeModel.Add(_feeModel);
                }
                return list_FeeModel;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }

        }
        public bool AddFeeSettings(FeeSettings model)
        {
            try
            {


                dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
                dal_stored_procedure1.name = "[dbo].[pr_add_fee_settings]";
                dal_manager dal_manager1 = new dal_manager();
                dal_stored_procedure1.add_parameter("@course_id", model.course_id, DbType.Int32, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@semester", model.semester, DbType.Int32, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@fee_type_id", model.fee_type_id, DbType.Int32, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@category_id", model.category_id, DbType.String, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@fee", model.fee, DbType.Decimal, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@due_date", model.due_date, DbType.Date, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@created_by",10, DbType.Int32, ParameterDirection.Input);


                dal_manager1.execute_nonquery(dal_stored_procedure1);
                //int i= (int)dal_stored_procedure1.get_params()["@return_val"].param_value;
            }
            catch (Exception ex)
            {

                throw;
            }
            return true;
        }
        public List<FeeSettings> Get_All_FeeSettings_List()
        {

            List<FeeSettings> list_FeeModel = new List<FeeSettings>();
            try
            {

                dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
                dal_stored_procedure1.name = "[dbo].[pr_get_fee_settings_list]";               
                dal_manager dal_manager1 = new dal_manager();
                DataSet ds_staff = dal_manager1.execute_dataset(dal_stored_procedure1);

                foreach (DataRow row in ds_staff.Tables[0].Rows)
                {
                    FeeSettings _feeModel = new FeeSettings();
                    _feeModel.fee_settings_id = Convert.ToInt32(row["fee_settings_id"]);
                    _feeModel.fee = Convert.ToDecimal(row["fee"]);
                    _feeModel.semester = Convert.ToInt32(row["semester"]);
                   _feeModel.course.course_name = Convert.ToString(row["course_name"]);
                    _feeModel.studentCategory.category_name= Convert.ToString(row["category_name"]);
                    _feeModel.feetype.fee_type = row["fee_type"].ToString();                   
                    _feeModel.due_date = Convert.ToDateTime(row["due_date"]);
                    list_FeeModel.Add(_feeModel);
                }
                return list_FeeModel;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }

        }

        public FeeSettings Get_Fee_settings_By_id(int fee_settings_id)
        {

            try
            {
                FeeSettings feeSettings = new FeeSettings();

                dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
                dal_stored_procedure1.name = "[dbo].[pr_get_fee_settings_by_id]";
                dal_stored_procedure1.add_parameter("@fee_settings_id", fee_settings_id, DbType.Int32, ParameterDirection.Input);
                dal_manager dal_manager1 = new dal_manager();
                DataSet ds_staff = dal_manager1.execute_dataset(dal_stored_procedure1);
                foreach (DataRow row in ds_staff.Tables[0].Rows)
                {
                    feeSettings.fee_settings_id =Convert.ToInt32(row["fee_settings_id"]);
                    feeSettings.course_id = Convert.ToInt32(row["course_id"]);
                    feeSettings.semester = Convert.ToInt32(row["semester"]);
                    feeSettings.fee_type_id = Convert.ToInt32(row["fee_type_id"]);
                    feeSettings.studentCategory.category_id = Convert.ToInt32(row["student_category_id"]);
                    feeSettings.fee = Convert.ToDecimal(row["fee"]);
                    feeSettings.due_date = Convert.ToDateTime(row["due_date"]);                   

                }
                return feeSettings;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }

        }
        public bool UpdateFeeSettings(FeeSettings model,int id)
        {
            try
            {


                dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
                dal_stored_procedure1.name = "[dbo].[pr_update_fee_settings]";
                dal_manager dal_manager1 = new dal_manager();
                dal_stored_procedure1.add_parameter("@fee_settings_is", id, DbType.Int32, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@course_id", model.course_id, DbType.Int32, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@semester", model.semester, DbType.Int32, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@fee_type_id", model.fee_type_id, DbType.Int32, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@category_id", model.category_id, DbType.String, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@fee", model.fee, DbType.Decimal, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@due_date", model.due_date, DbType.Date, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@created_by", 10, DbType.Int32, ParameterDirection.Input);


                dal_manager1.execute_nonquery(dal_stored_procedure1);
                //int i= (int)dal_stored_procedure1.get_params()["@return_val"].param_value;
            }
            catch (Exception ex)
            {

                throw;
            }
            return true;
        }
        public Boolean  DelateFeeSettings(int fee_settings_id)
        {

            try
            {
                

                dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
                dal_stored_procedure1.name = "[dbo].[pr_delete_fee_settings_by_id]";
                dal_stored_procedure1.add_parameter("@fee_settings_id", fee_settings_id, DbType.Int32, ParameterDirection.Input);
                dal_manager dal_manager1 = new dal_manager();
                dal_manager1.execute_nonquery(dal_stored_procedure1);
                return true;

            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
            finally
            {
            }

        }
    }
}