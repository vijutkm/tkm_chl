﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TKM_CENTRE_FOR_HIGHER_LEARNING.Models
{
    public class Course
    {
        public int course_id { get; set; }
        [Display(Name = "Course Name")]
        public string course_name { get; set; }
    }


    public class Course_Dal
    {
        public SelectList CourseList { get; set; }
        public List<Course> Course_List()
        {
            List<Course> list_course = new List<Course>();
            try
            {

                dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
                dal_stored_procedure1.name = "[dbo].[pr_get_course_list]";
                dal_manager dal_manager1 = new dal_manager();
                DataSet ds_student = dal_manager1.execute_dataset(dal_stored_procedure1);
                foreach (DataRow row in ds_student.Tables[0].Rows)
                {
                    Course course = new Course();
                    course.course_id = Convert.ToInt32(row["course_id"]);
                    course.course_name = row["course_name"].ToString();


                    list_course.Add(course);
                }
                return list_course;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }

        }
    }

}