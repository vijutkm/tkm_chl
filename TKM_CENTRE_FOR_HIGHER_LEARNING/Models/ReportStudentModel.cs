﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web;

namespace TKM_CENTRE_FOR_HIGHER_LEARNING.Models
{
    public class ReportStudentModel
    {
        [DisplayName("Course")]
        public int Course_Id { get; set; }
        [Required(ErrorMessage = "* Required")]
        [DisplayName("Course")]
        public string Course_name { get; set; }

        [Required(ErrorMessage = "* Required")]
        [Range(1, 6, ErrorMessage = "Please use values between 1 to 6")]
        [DisplayName("Semester")]
        public int Semester { get; set; }


    }
}