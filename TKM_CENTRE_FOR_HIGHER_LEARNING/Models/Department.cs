﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TKM_CENTRE_FOR_HIGHER_LEARNING.Models
{
    public class Department
    {
        public int department_id { get; set; }
        [Display(Name = "Department Name")]
        public string department_name { get; set; }

       
    }

    public class departmentdal
    {
        public SelectList DepartmentList { get; set; }
        public List<Department> department_List()
        {
            List<Department> list_department = new List<Department>();
            try
            {

                dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
                dal_stored_procedure1.name = "[dbo].[pr_get_department_list]";
                dal_manager dal_manager1 = new dal_manager();
                 DataSet ds_student = dal_manager1.execute_dataset(dal_stored_procedure1);
                foreach (DataRow row in ds_student.Tables[0].Rows)
                {
                    Department department = new Department();
                    department.department_id =Convert.ToInt32(row["department_id"]);
                    department.department_name= row["department_name"].ToString();


                    list_department.Add(department);
                }
                return list_department;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }

        }
    }
}