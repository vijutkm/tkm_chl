﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using TKM_CENTRE_FOR_HIGHER_LEARNING.ViewModel;

namespace TKM_CENTRE_FOR_HIGHER_LEARNING.Models
{
    public class Salary
    {
        [Display(Name = "Id")]
        public int staff_id { get; set; }

        [Display(Name = "Name")]
        public string staff_name { get; set; }

        [Display(Name = "Basic + DA")]
        public decimal basic { get; set; }

        [Display(Name = "HRA")]
        public decimal hra { get; set; }

        [Display(Name = "CCA")]
        public decimal cca { get; set; }

        [Display(Name = "Med Allowance")]
        public decimal medical_allowance { get; set; }

        [Display(Name = "Food Allowance")]
        public decimal food_allowance { get; set; }

        [Display(Name = "Educational Allowance")]
        public decimal educational_allowance { get; set; }

        [Display(Name = "Transport Allowance")]
        public decimal transport_allowance { get; set; }

        [Display(Name = "Telephone & Internet Charges")]
        public decimal telephone_internet_charges { get; set; }

        [Display(Name = "Conveyance Allowance")]
        public decimal conveyance_allowance { get; set; }

        [Display(Name = "Special Allowance")]
        public decimal special_allowance { get; set; }

        [Display(Name = "Bonus")]
        public decimal bonus { get; set; }

        [Display(Name = "EPF")]
        public decimal epf { get; set; }

        [Display(Name = "ESI")]
        public decimal esi { get; set; }

        [Display(Name = "Labour Welfare Fund")]
        public decimal labour_welfare_fund { get; set; }

        [Display(Name = "Salary Advanced Deduction")]
        public decimal salary_advanced_deduction { get; set; }

        [Display(Name = "Leave Deduction")]
        public decimal leave_deduction { get; set; }

        [Display(Name = "Professional Tax")]
        public decimal professional_tax { get; set; }

        [Display(Name = "TDS")]
        public decimal tds { get; set; }

        [Display(Name = "Created At")]
        public DateTime created_on { get; set; }


        [Display(Name = "Gross Salary")]
        public decimal gross_salary { get; set; }

        [Display(Name = "Gross Deduction")]
        public decimal gross_deduction { get; set; }

        [Display(Name = "Net Salary")]
        public decimal net_salary { get; set; }

        [Display(Name = "Created By")]
        public int created_by { get; set; }

        [Display(Name = "Month")]
        [Required(ErrorMessage = "month is required.")]
        public int SelectedMonth
        {
            get;
            set;
        }
        [Display(Name = "Year")]
        [Required(ErrorMessage = "year is required.")]
        public int SelectedYear
        {
            get;
            set;
        }

       
        private SqlConnection con;
        private void connection()
        {
            string constring = ConfigurationManager.AppSettings["conStr"].ToString();
            con = new SqlConnection(constring);
        }
        public Salary GetStaffSalary(int staff_id)
        {

            try
            {
                Salary salary = new Salary();

                dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
                dal_stored_procedure1.name = "[dbo].[pr_get_salary_details_by_staff_id]";
                dal_stored_procedure1.add_parameter("@staff_id", staff_id, DbType.String, ParameterDirection.Input);
                dal_manager dal_manager1 = new dal_manager();
                DataSet ds_staff_salary = dal_manager1.execute_dataset(dal_stored_procedure1);
                if(ds_staff_salary.Tables[0].Rows.Count==1)
                {
                    salary.staff_id =int.Parse( ds_staff_salary.Tables[0].Rows[0]["staff_id"].ToString());
                    salary.basic = decimal.Parse(ds_staff_salary.Tables[0].Rows[0]["basic"].ToString());
                    salary.hra = decimal.Parse(ds_staff_salary.Tables[0].Rows[0]["hra"].ToString());
                    salary.cca = decimal.Parse(ds_staff_salary.Tables[0].Rows[0]["cca"].ToString());
                    salary.epf = decimal.Parse(ds_staff_salary.Tables[0].Rows[0]["epf"].ToString());
                    salary.medical_allowance = decimal.Parse(ds_staff_salary.Tables[0].Rows[0]["medical_allowance"].ToString());
                    salary.food_allowance = decimal.Parse(ds_staff_salary.Tables[0].Rows[0]["food_allowance"].ToString());
                    salary.professional_tax = decimal.Parse(ds_staff_salary.Tables[0].Rows[0]["professional_tax"].ToString());
                    salary.esi = decimal.Parse(ds_staff_salary.Tables[0].Rows[0]["esi"].ToString());
                    salary.educational_allowance = decimal.Parse(ds_staff_salary.Tables[0].Rows[0]["educational_allowance"].ToString());
                    salary.transport_allowance = decimal.Parse(ds_staff_salary.Tables[0].Rows[0]["transport_allowance"].ToString());
                    salary.labour_welfare_fund = decimal.Parse(ds_staff_salary.Tables[0].Rows[0]["labour_welfare_fund"].ToString());
                    salary.conveyance_allowance = decimal.Parse(ds_staff_salary.Tables[0].Rows[0]["conveyance_allowance"].ToString());
                    salary.special_allowance = decimal.Parse(ds_staff_salary.Tables[0].Rows[0]["special_allowance"].ToString());
                    salary.telephone_internet_charges = decimal.Parse(ds_staff_salary.Tables[0].Rows[0]["telephone_internet_charges"].ToString());
                    salary.gross_salary = decimal.Parse(ds_staff_salary.Tables[0].Rows[0]["gross_salary"].ToString());
                    salary.gross_deduction = decimal.Parse(ds_staff_salary.Tables[0].Rows[0]["gross_deduction"].ToString());
                    salary.net_salary = decimal.Parse(ds_staff_salary.Tables[0].Rows[0]["net_salary"].ToString());
                }
                return salary;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }

        }



        public bool UpdateSalary(Salary salary,int login_user_id)
        {
 
            
            try
            {

                dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
                dal_stored_procedure1.name = "[dbo].[pr_update_staff_salary_by_staff_id]";
                dal_stored_procedure1.add_parameter("@staff_id", salary.staff_id, DbType.Int32, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@leave_deduction", salary.leave_deduction, DbType.Decimal, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@bonus", salary.bonus, DbType.Decimal, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@salary_advanced_deduction", salary.salary_advanced_deduction, DbType.Decimal, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@tds", salary.tds, DbType.Decimal, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@gross_salary", salary.gross_salary, DbType.Decimal, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@gross_deduction", salary.gross_deduction, DbType.Decimal, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@net_salary", salary.net_salary, DbType.Decimal, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@login_user_id", login_user_id, DbType.Int32, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@month", salary.SelectedMonth, DbType.Int32, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@year", salary.SelectedYear, DbType.Int32, ParameterDirection.Input);
                dal_manager dal_manager1 = new dal_manager();
                DataSet ds_staff_salary = dal_manager1.execute_dataset(dal_stored_procedure1);
           
                return true;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }

        }


        public bool AddSalary(Salary smodel)
        {
            connection();
            SqlCommand cmd = new SqlCommand("pr_add_salary_settings", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@staff_id", smodel.staff_id);
            cmd.Parameters.AddWithValue("@basic", smodel.basic);
            cmd.Parameters.AddWithValue("@hra", smodel.hra);
            cmd.Parameters.AddWithValue("@cca", smodel.cca);
            cmd.Parameters.AddWithValue("@epf", smodel.epf);
            cmd.Parameters.AddWithValue("@medical_allowance", smodel.medical_allowance);
            cmd.Parameters.AddWithValue("@food_allowance", smodel.food_allowance);
            cmd.Parameters.AddWithValue("@professional_tax", smodel.professional_tax);
            cmd.Parameters.AddWithValue("@esi", smodel.esi);
            cmd.Parameters.AddWithValue("@educational_allowance", smodel.educational_allowance);
            cmd.Parameters.AddWithValue("@transport_allowance", smodel.transport_allowance);
            cmd.Parameters.AddWithValue("@labour_welfare_fund", smodel.labour_welfare_fund);
            cmd.Parameters.AddWithValue("@conveyance_allowance", smodel.conveyance_allowance);
            cmd.Parameters.AddWithValue("@special_allowance", smodel.special_allowance);
            cmd.Parameters.AddWithValue("@telephone_internet_charges", smodel.telephone_internet_charges);
            cmd.Parameters.AddWithValue("@gross_salary", smodel.gross_salary);
            cmd.Parameters.AddWithValue("@gross_deduction", smodel.gross_deduction);
            cmd.Parameters.AddWithValue("@net_salary", smodel.net_salary);
            cmd.Parameters.AddWithValue("@created_by", smodel.created_by);
            con.Open();
            int i = cmd.ExecuteNonQuery();
            con.Close();

            if (i >= 1)
                return true;
            else
                return false;
        }



        public class SalaryDal
        {
            public List<Salary> Get_salary_List(SalaryViewModel vm)
            {

                List<Salary> list_salary = new List<Salary>();
                try
                {

                    dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
                    dal_stored_procedure1.name = "[dbo].[pr_get_salary_list]";
                    dal_stored_procedure1.add_parameter("@salary_month", vm.salary.SelectedMonth, DbType.Int32, ParameterDirection.Input);
                    dal_stored_procedure1.add_parameter("@salary_year", vm.salary.SelectedYear, DbType.Int32, ParameterDirection.Input);

                    dal_manager dal_manager1 = new dal_manager();
                    DataSet ds_staff = dal_manager1.execute_dataset(dal_stored_procedure1);

                    foreach (DataRow row in ds_staff.Tables[0].Rows)
                    {
                        Salary _salaryModel = new Salary();
                        _salaryModel.staff_name = Convert.ToString(row["name"]);
                        _salaryModel.staff_id = Convert.ToInt32(row["staff_id"]);
                        _salaryModel.basic = Convert.ToDecimal(row["basic"]);

                        _salaryModel.hra = Convert.ToDecimal(row["hra"]);
                        _salaryModel.cca = Convert.ToDecimal(row["cca"]);

                        _salaryModel.medical_allowance = Convert.ToDecimal(row["medical_allowance"]);
                        _salaryModel.food_allowance = Convert.ToDecimal(row["food_allowance"]);
                        _salaryModel.educational_allowance = Convert.ToDecimal(row["educational_allowance"]);
                        _salaryModel.transport_allowance = Convert.ToDecimal(row["transport_allowance"]);
                        _salaryModel.telephone_internet_charges = Convert.ToDecimal(row["telephone_internet_charges"]);
                        _salaryModel.conveyance_allowance = Convert.ToDecimal(row["conveyance_allowance"]);
                        _salaryModel.special_allowance = Convert.ToDecimal(row["special_allowance"]);
                        _salaryModel.epf = Convert.ToDecimal(row["epf"]);
                        _salaryModel.esi = Convert.ToDecimal(row["esi"]);
                        _salaryModel.labour_welfare_fund = Convert.ToDecimal(row["labour_welfare_fund"]);
                        _salaryModel.professional_tax = Convert.ToDecimal(row["professional_tax"]);
                        _salaryModel.gross_salary = _salaryModel.basic + _salaryModel.hra + _salaryModel.cca+_salaryModel.medical_allowance +
                                                    _salaryModel.food_allowance + _salaryModel.educational_allowance + _salaryModel.transport_allowance +
                                                     _salaryModel.telephone_internet_charges + _salaryModel.conveyance_allowance + _salaryModel.special_allowance;

                        _salaryModel.gross_deduction = _salaryModel.epf + _salaryModel.esi + _salaryModel.professional_tax + _salaryModel.labour_welfare_fund;
                        _salaryModel.net_salary = _salaryModel.gross_salary - _salaryModel.gross_deduction;
                        list_salary.Add(_salaryModel);
                    }
                    return list_salary;

                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                }

            }



        }

        }




}