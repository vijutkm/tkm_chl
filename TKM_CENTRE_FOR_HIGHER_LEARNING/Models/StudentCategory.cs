﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TKM_CENTRE_FOR_HIGHER_LEARNING.Models
{
    public class StudentCategory
    {
        public int category_id { get; set; }
        [Display(Name = "Category Name")]
        public string category_name { get; set; }

    }


    public class CategoryDal
    {
        public SelectList categoryList { get; set; }
        public List<StudentCategory> category_List()
        {
            List<StudentCategory> list_category = new List<StudentCategory>();
            try
            {

                dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
                dal_stored_procedure1.name = "[dbo].[pr_get_category_list]";
                dal_manager dal_manager1 = new dal_manager();
                DataSet ds_student = dal_manager1.execute_dataset(dal_stored_procedure1);
                foreach (DataRow row in ds_student.Tables[0].Rows)
                {
                    StudentCategory category = new StudentCategory();
                    category.category_id = Convert.ToInt32(row["category_id"]);
                    category.category_name = row["category_name"].ToString();


                    list_category.Add(category);
                }
                return list_category;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }

        }
    }
}