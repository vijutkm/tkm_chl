﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TKM_CENTRE_FOR_HIGHER_LEARNING.Models
{
    public class Roles
    {

        public int role_id { get; set; }
        [Display(Name = "Role")]
        public string role_name { get; set; }
    }



    public class Roledal
    {
        public SelectList RoleList { get; set; }
        public List<Roles> Role_List()
        {
            List<Roles> list_Role = new List<Roles>();
            try
            {

                dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
                dal_stored_procedure1.name = "[dbo].[pr_get_role_list]";
                dal_manager dal_manager1 = new dal_manager();
                DataSet ds_roles = dal_manager1.execute_dataset(dal_stored_procedure1);
                foreach (DataRow row in ds_roles.Tables[0].Rows)
                {
                    Roles role = new Roles();
                    role.role_id = Convert.ToInt32(row["role_id"]);
                    role.role_name = row["role_name"].ToString();


                    list_Role.Add(role);
                }
                return list_Role;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }

        }
    }
}