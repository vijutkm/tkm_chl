﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TKM_CENTRE_FOR_HIGHER_LEARNING.Models
{
    public class ReportExpenseModel
    {
        [DisplayName( "From Date")]
        [Required(ErrorMessage = "From date is required.")]
        public DateTime from_date { get; set; }

        [DisplayName( "To Date")]
        [Required(ErrorMessage = "To date is required.")]
        public DateTime to_date { get; set; }

        [DisplayName("Payment Category")]
        public int payment_category_id { get; set; }
    }
}