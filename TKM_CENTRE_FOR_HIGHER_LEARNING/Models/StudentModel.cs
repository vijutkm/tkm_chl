﻿using System;
using System.Data;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Collections.Generic;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations.Schema;
using TKM_CENTRE_FOR_HIGHER_LEARNING.ViewModel;

namespace TKM_CENTRE_FOR_HIGHER_LEARNING.Models
{
    [MetadataType(typeof(StudentModel))]
    public partial class StudentModel
    {
        public string strStudentId { get; set; }

        [Required(ErrorMessage = " Required")]
        [DisplayName("First Name")]
        [RegularExpression(@"^[a-zA-Z_ ]+$", ErrorMessage = "Use letters only")]
        public string strFirstName { get; set; }

        [DisplayName("Last Name")]
        [Required(ErrorMessage = " Required")]
        [RegularExpression(@"^[a-zA-Z_ ]+$", ErrorMessage = "Use letters only")]
        public string strLastName { get; set; }

       

        [DataObjectField(false)]
        [DisplayName("Date of Birth")]
        public DateTime dtDOB { get; set; }


        [Required(ErrorMessage = "Required")]
        [DisplayName("Student code")]
        public string student_code { get; set; }

        [DataObjectField(false)]
        [DisplayName("Date of Birth")]
       
        public string dtDOBedit { get; set; }

        [DisplayName("Gender")]
        public bool boolGender { get; set; }

        [Required(ErrorMessage = "Required")]
        [RegularExpression(@"^([0-9]{10,14})$", ErrorMessage = "Invalid Mobile Number.")]
        [DisplayName("Mobile Number")]
        public string strMobileNumber { get; set; }

        //[Required(ErrorMessage = "* Required")]
        //[DisplayName("BatchId")]
        //public int intBatchId { get; set; }

        [DisplayName("Course")]
        public int Course_Id { get; set; }

        [Required(ErrorMessage = "Required")]
        [DisplayName("Course")]
        public string Course_name { get; set; }


        [DisplayName("Category")]
        public string category_name { get; set; }


        [Required(ErrorMessage = "Required")]
        [Range(1, 6, ErrorMessage = "Please use values between 1 to 6")]
        [DisplayName("Semester")]
        public int Semester { get; set; }
        [DisplayName("Year")]
        public int joining_year { get; set; }

        public int created_by { get; set; }
        public DateTime created_on { get; set; }
        [Display(Name = "E-Mail")]
        [RegularExpression(@"[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$", ErrorMessage = "Enter valid email")]
        [Required(ErrorMessage = "Email-id is required.")]
        public string email_id { get; set; }

        [DisplayName("Category")]
        public int category_id { get; set; }

      

    }
    public class StudentDal 
    {
     

        public List<StudentModel> StudentList(int page_no)
        {
            List<StudentModel> list_student_model = new List<StudentModel>();
            try
            {

                dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
                dal_stored_procedure1.name = "[dbo].[pr_get_student_list]";
                dal_manager dal_manager1 = new dal_manager();
                dal_stored_procedure1.add_parameter("@page_no", page_no, DbType.Int32, ParameterDirection.Input);
                DataSet ds_student = dal_manager1.execute_dataset(dal_stored_procedure1);
                foreach (DataRow row in ds_student.Tables[0].Rows)
                {
                    StudentModel student_model = new StudentModel();
                    student_model.strStudentId = row["student_id"].ToString();
                    student_model.strFirstName = row["first_name"].ToString();
                    student_model.strLastName = row["last_name"].ToString();
                    student_model.strMobileNumber = row["mobile_number"].ToString();
                    student_model.dtDOB =(DateTime) row["dob"];
                    student_model.Course_name= row["course_name"].ToString();
                    student_model.category_name = row["category_name"].ToString();
                    student_model.Semester = Convert.ToInt32(row["semester"]);
                    student_model.boolGender = Convert.ToBoolean(row["gender"]);

                    list_student_model.Add(student_model);
                }
                return list_student_model;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }

        }
        public List<StudentModel> StudentListById(string id)
        {
            List<StudentModel> list_student_model = new List<StudentModel>();
            try
            {

                dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
                dal_stored_procedure1.name = "[dbo].[pr_get_student_details_by_id]";
                dal_manager dal_manager1 = new dal_manager();
                dal_stored_procedure1.add_parameter("@student_id", id, DbType.String, ParameterDirection.Input);
                DataSet ds_student = dal_manager1.execute_dataset(dal_stored_procedure1);
                foreach (DataRow row in ds_student.Tables[0].Rows)
                {
                    StudentModel student_model = new StudentModel();
                    student_model.strStudentId = row["student_id"].ToString();
                    student_model.strFirstName = row["first_name"].ToString();
                    student_model.strLastName = row["last_name"].ToString();
                    student_model.strMobileNumber = row["mobile_number"].ToString();
                    student_model.dtDOB = (DateTime)row["dob"];
                   
                    // student_model.Course_Id = Convert.ToInt32(row["course_id"]);
                    student_model.Course_name = row["course_name"].ToString();
                    student_model.Semester = Convert.ToInt32(row["semester"]);
                    student_model.boolGender = Convert.ToBoolean(row["gender"]);

                    list_student_model.Add(student_model);
                }
                return list_student_model;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }

        }

        public bool AddStudent(StudentModel smodel)
        {
            dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
            dal_stored_procedure1.name = "[dbo].[AddNewStudent]";
            dal_manager dal_manager1 = new dal_manager();
            dal_stored_procedure1.add_parameter("@first_name", smodel.strFirstName, DbType.String, ParameterDirection.Input);
            dal_stored_procedure1.add_parameter("@last_name", smodel.strLastName, DbType.String, ParameterDirection.Input);
            dal_stored_procedure1.add_parameter("@mobile_number", smodel.strMobileNumber, DbType.String, ParameterDirection.Input);
            dal_stored_procedure1.add_parameter("@boolGender", smodel.boolGender, DbType.Boolean, ParameterDirection.Input);
            dal_stored_procedure1.add_parameter("@dob", smodel.dtDOB, DbType.Date, ParameterDirection.Input);
            dal_stored_procedure1.add_parameter("@student_code", smodel.student_code, DbType.String, ParameterDirection.Input);
            dal_stored_procedure1.add_parameter("@Course_Id", smodel.Course_Id, DbType.Int32, ParameterDirection.Input);
            dal_stored_procedure1.add_parameter("@category_id", smodel.category_id, DbType.Int32, ParameterDirection.Input);
            dal_stored_procedure1.add_parameter("@Semester", smodel.Semester, DbType.Int32, ParameterDirection.Input);
            dal_stored_procedure1.add_parameter("@joining_year", smodel.joining_year, DbType.Int32, ParameterDirection.Input);
            dal_stored_procedure1.add_parameter("@email_id", smodel.email_id, DbType.String, ParameterDirection.Input);
            dal_manager1.execute_nonquery(dal_stored_procedure1);
            //int i= (int)dal_stored_procedure1.get_params()["@return_val"].param_value;
            
                return true;            
        }
        public bool UpdateStudent(StudentModel smodel)
        {
            try
            {

          
            dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
            dal_stored_procedure1.name = "[dbo].[UpdateNewStudent]";
            dal_manager dal_manager1 = new dal_manager();
            dal_stored_procedure1.add_parameter("@StudentId", smodel.strStudentId, DbType.String, ParameterDirection.Input);
            dal_stored_procedure1.add_parameter("@first_name", smodel.strFirstName, DbType.String, ParameterDirection.Input);
            dal_stored_procedure1.add_parameter("@last_name", smodel.strLastName, DbType.String, ParameterDirection.Input);
            dal_stored_procedure1.add_parameter("@mobile_number", smodel.strMobileNumber, DbType.String, ParameterDirection.Input);
            dal_stored_procedure1.add_parameter("@boolGender", smodel.boolGender, DbType.Boolean, ParameterDirection.Input);
            dal_stored_procedure1.add_parameter("@CourseId", smodel.Course_Id, DbType.Int32, ParameterDirection.Input);
            dal_stored_procedure1.add_parameter("@category_id", smodel.category_id, DbType.Int32, ParameterDirection.Input);
            dal_stored_procedure1.add_parameter("@Semester", smodel.Semester, DbType.Int32, ParameterDirection.Input);
            dal_stored_procedure1.add_parameter("@dob", smodel.dtDOBedit, DbType.Date, ParameterDirection.Input);

            dal_manager1.execute_nonquery(dal_stored_procedure1);
                //int i= (int)dal_stored_procedure1.get_params()["@return_val"].param_value;
            }
            catch (Exception)
            {

                throw;
            }
            return true;
        }
        public bool deleteStudent(string id)
        {
            try
            {


                dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
                dal_stored_procedure1.name = "[dbo].[DeleteStudent]";
                dal_manager dal_manager1 = new dal_manager();
                dal_stored_procedure1.add_parameter("@StudentId", id, DbType.String, ParameterDirection.Input);               

                dal_manager1.execute_nonquery(dal_stored_procedure1);
                //int i= (int)dal_stored_procedure1.get_params()["@return_val"].param_value;
            }
            catch (Exception e)
            {

                return false;
            }
            return true;
        }


        public StudentModel GetStudent_id(string student_id)
        {

            try
            {
                StudentModel student_model = new StudentModel();

                dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
                dal_stored_procedure1.name = "[dbo].[pr_get_student_details_by_id]";
                dal_stored_procedure1.add_parameter("@student_id", student_id, DbType.String, ParameterDirection.Input);
                dal_manager dal_manager1 = new dal_manager();
                DataSet ds_staff = dal_manager1.execute_dataset(dal_stored_procedure1);
                foreach (DataRow row in ds_staff.Tables[0].Rows)
                {
                    student_model.strStudentId = row["student_id"].ToString();
                    student_model.strFirstName = row["first_name"].ToString();
                    student_model.strLastName = row["last_name"].ToString();
                    student_model.strMobileNumber = row["mobile_number"].ToString();
                    student_model.dtDOBedit = ((DateTime)row["dob"]).ToShortDateString();
                    student_model.Course_Id = Convert.ToInt32(row["course_id"]);
                    student_model.Course_name = row["course_name"].ToString();
                    student_model.category_id = Convert.ToInt32(row["category_id"]);
                    student_model.Semester = Convert.ToInt32(row["semester"]);
                    student_model.boolGender = Convert.ToBoolean(row["gender"]);

                }
                return student_model;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }

        }
        public EntrollViewModel GetAllStudentList(EntrollViewModel vm)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            EntrollViewModel vmobj = new EntrollViewModel();
           List< EntrollViewModel> vmoList = new List <EntrollViewModel>();
            try
            {

                dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
                dal_stored_procedure1.name = "[dbo].[pr_get_student_list_course_sem]";
                dal_manager dal_manager1 = new dal_manager();
                dal_stored_procedure1.add_parameter("@course_id", vm.course.course_id, DbType.Int32, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@semester", vm.entroll.semester, DbType.Int32, ParameterDirection.Input);
                DataSet ds_student = dal_manager1.execute_dataset(dal_stored_procedure1);
               

                foreach (DataRow row in ds_student.Tables[0].Rows)
                {                  

                    items.Add(new SelectListItem
                    {
                        Text = row["first_name"].ToString(),
                        Value = row["student_id"].ToString()
                    });

                }
                foreach (DataRow row in ds_student.Tables[1].Rows)
                {
                    EntrollViewModel obj = new EntrollViewModel();

                    obj.student_name = row["student_name"].ToString();
                    obj.student_id = row["student_id"].ToString();
                    obj.due =Convert.ToDecimal( row["Due"].ToString());
                    obj.due_status= Convert.ToInt32(row["Due_status"].ToString());
                    vmoList.Add(obj);

                }


                vmobj.StudentNames = items;
                vmobj.vmList = vmoList;

                return vmobj;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }

        }

    }
}