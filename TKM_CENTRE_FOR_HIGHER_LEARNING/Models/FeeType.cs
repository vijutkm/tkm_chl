﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TKM_CENTRE_FOR_HIGHER_LEARNING.Models
{
    public class FeeType
    {
        public int fee_type_id { get; set; }
        [Display(Name = "Fee Type")]
        public string fee_type { get; set; }
        public int status { get; set; }
    }
    public class FeeTypeDAL
    {
        public SelectList feeTypeList { get; set; }
        public List<FeeType> FeeType_list()
        {
            List<FeeType> feeType = new List<FeeType>();
            try
            {

                dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
                dal_stored_procedure1.name = "[dbo].[pr_get_fee_category_list]";
                dal_manager dal_manager1 = new dal_manager();
                DataSet ds_student = dal_manager1.execute_dataset(dal_stored_procedure1);
                foreach (DataRow row in ds_student.Tables[0].Rows)
                {
                    FeeType _FeeType = new FeeType();
                    _FeeType.fee_type_id = Convert.ToInt32(row["fee_type_id"]);
                    _FeeType.fee_type = row["fee_type"].ToString();
                    feeType.Add(_FeeType);
                }
                return feeType;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }

        }
    }
}