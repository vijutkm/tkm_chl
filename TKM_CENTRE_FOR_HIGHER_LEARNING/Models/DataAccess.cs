﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Data;
using System.Data.Odbc;
using System.Data.SqlClient;
using System.Data.OleDb;



namespace TKM_CENTRE_FOR_HIGHER_LEARNING.Models
{

    /// <summary>
    /// Holds Parameter value
    /// </summary>
    public struct dal_param
    {

        public string param_name;
        public object param_value;
        public DbType param_data_type;
        public ParameterDirection param_direction;
        /// <summary>
        /// Set parameter value
        /// </summary>
        /// <param name="param_name"></param>
        /// <param name="param_value"></param>
        /// <param name="param_data_type"></param>
        /// <param name="param_direction"></param>
        public dal_param(string param_name, object param_value, DbType param_data_type, ParameterDirection param_direction)
        {
            this.param_name = param_name;
            this.param_value = param_value;
            this.param_data_type = param_data_type;
            this.param_direction = param_direction;
        }

    }
    /// <summary>
    /// Parameter collection
    /// </summary>
    public class dal_params : List<dal_param>
    {
        /// <summary>
        /// Indexer to set and get parameter name
        /// </summary>
        /// <param name="param_name"></param>
        /// <returns></returns>
        public dal_param this[string param_name]
        {
            get
            {
                dal_param ret_dal_param = new dal_param();
                foreach (dal_param dal_param1 in this)
                {
                    if (dal_param1.param_name == param_name)
                        ret_dal_param = dal_param1;
                }
                return ret_dal_param;
            }

            set { this[param_name] = value; }

        }

    }
    /// <summary>
    /// Executable stored procedure
    /// Holds all the values for a stored procedure needee to execute
    /// </summary>

    public class dal_stored_procedure
    {
        dal_params parameters = new dal_params();

        private string _name;
        /// <summary>
        /// gets and sets Stored Procedure name
        /// </summary>
        public string name
        {

            get { return _name; }
            set { _name = value; }
        }
        /// <summary>
        /// Adds Parameter
        /// </summary>
        /// <param name="param_name"></param>
        /// <param name="param_value"></param>
        /// <param name="param_data_type"></param>
        /// <param name="param_direction"></param>
        public void add_parameter(string param_name, object param_value, DbType param_data_type, ParameterDirection param_direction)

        {

            parameters.Add(new dal_param(param_name, param_value, param_data_type, param_direction));

        }
        /// <summary>
        /// Returns Parameter Collection
        /// </summary>
        /// <returns>Parameter Collection</returns>
        public dal_params get_params()
        {
            return parameters;
        }
        /// <summary>
        /// Sets Parameter Collection
        /// </summary>
        /// <param name="parameters"></param>
        public void set_params(dal_params parameters)
        {
            this.parameters = parameters;
        }
    }
    /// <summary>
    /// Enumeration holds all the providers
    /// </summary>
    public enum dal_data_provider
    {
        oracle,
        sql_server,
        oledb,
        odbc
    }
    /// <summary>
    /// DAL Manager interface includes all the database operations for all the Database providers
    /// </summary>
    public interface IDALManager
    {
        /// <summary>
        /// gets sets Provider Type
        /// </summary>
        dal_data_provider dal_data_provider
        {
            get;
            set;
        }
        /// <summary>
        /// gets sets Connection String
        /// </summary>
        string connection_string
        {
            get;
            set;
        }
        /// <summary>
        /// Read only Connection object
        /// </summary>
        IDbConnection Connection
        {
            get;
        }
        /// <summary>
        /// Read only DataReader object
        /// </summary>
        IDataReader idata_reader1
        {
            get;
        }
        /// <summary>
        /// Read only Command object
        /// </summary>
        IDbCommand idb_command1
        {
            get;
        }

        /// <summary>
        /// Initializes the database operations
        /// </summary>
        void initialize();
        /// <summary>
        /// Initializes transaction
        /// </summary>
        void begin_transaction();
        /// <summary>
        /// Commits the current transaction
        /// </summary>
        void commit_transaction();
        /// <summary>
        /// Creates Executable command object by initializing the parameters 
        /// </summary>
        /// <param name="objStoredProcedure"></param>
        void prepare_command(dal_stored_procedure objStoredProcedure);
        /// <summary>
        /// Returns Datareader with respect to the providers
        /// </summary>
        /// <param name="objStoredProcedure"></param>
        /// <returns></returns>
        IDataReader execute_reader(dal_stored_procedure objStoredProcedure);
        /// <summary>
        /// Executes the storedprocedure object and Returns Dataset with respect to the providers
        /// </summary>
        /// <param name="objStoredProcedure"></param>
        /// <returns></returns>
        DataSet execute_dataset(dal_stored_procedure objStoredProcedure);
        /// <summary>
        /// Executes the storedprocedure object and Returns first column of the first row 
        /// </summary>
        /// <param name="objStoredProcedure"></param>
        /// <returns></returns>
        object execute_scalar(dal_stored_procedure objStoredProcedure);
        /// <summary>
        /// Executes the storedprocedure object and returns the rows affected
        /// </summary>
        /// <param name="objStoredProcedure"></param>
        /// <returns></returns>
        int execute_nonquery(dal_stored_procedure objStoredProcedure);
        /// <summary>
        /// Closes the DataReader
        /// </summary>
        void close_reader();
        /// <summary>
        /// Closes the dal_manager object
        /// </summary>
        void Close();
        /// <summary>
        /// Removes memory space of the object
        /// </summary>
        void Dispose();
    }
    /// <summary>
    /// Factory methods which returns objects of required type
    /// </summary>
    public sealed class dal_manager_factory
    {
        private dal_manager_factory() { }
        /// <summary>
        /// Returns connection object with respect to provider type 
        /// </summary>
        /// <param name="dal_data_provider1"></param>
        /// <returns></returns>
        public static IDbConnection get_connection(dal_data_provider dal_data_provider1)
        {
            IDbConnection idb_connection1 = null;

            switch (dal_data_provider1)
            {
                case dal_data_provider.sql_server:
                    idb_connection1 = new SqlConnection();
                    break;
                case dal_data_provider.oledb:
                    idb_connection1 = new OleDbConnection();
                    break;
                case dal_data_provider.odbc:
                    idb_connection1 = new OdbcConnection();
                    break;
                //case dal_data_provider.oracle:
                //    idb_connection1 = new OracleConnection();
                //    break;
                default:
                    return null;
            }
            return idb_connection1;
        }
        /// <summary>
        /// Returns Command object with respect to provider type 
        /// </summary>
        /// <param name="dal_data_provider1"></param>
        /// <returns></returns>
        public static IDbCommand get_command(dal_data_provider dal_data_provider1)
        {
            switch (dal_data_provider1)
            {
                case dal_data_provider.sql_server:
                    return new SqlCommand();
                case dal_data_provider.oledb:
                    return new OleDbCommand();
                case dal_data_provider.odbc:
                    return new OdbcCommand();
                //case dal_data_provider.oracle:
                //    return new OracleCommand();
                default:
                    return null;
            }
        }
        /// <summary>
        /// Returns DataAdapter object with respect to provider type 
        /// </summary>
        /// <param name="dal_data_provider1"></param>
        /// <returns></returns>
        public static IDbDataAdapter get_data_adapter(dal_data_provider dal_data_provider1)
        {
            switch (dal_data_provider1)
            {
                case dal_data_provider.sql_server:
                    return new SqlDataAdapter();
                case dal_data_provider.oledb:
                    return new OleDbDataAdapter();
                case dal_data_provider.odbc:
                    return new OdbcDataAdapter();
                //case dal_data_provider.oracle:
                //    return new OracleDataAdapter();
                default:
                    return null;
            }
        }
        /// <summary>
        /// Returns Data Prameter Collection object with respect to provider type 
        /// </summary>
        /// <param name="dal_data_provider1"></param>
        /// <param name="params_count"></param>
        /// <returns></returns>
        public static IDbDataParameter[] get_parameters(dal_data_provider dal_data_provider1, int params_count)
        {
            IDbDataParameter[] idb_params = new IDbDataParameter[params_count];

            switch (dal_data_provider1)
            {
                case dal_data_provider.sql_server:
                    for (int ix = 0; ix < params_count; ++ix)
                    {
                        idb_params[ix] = new SqlParameter();
                    }
                    break;
                case dal_data_provider.oledb:
                    for (int ix = 0; ix < params_count; ++ix)
                    {
                        idb_params[ix] = new OleDbParameter();
                    }
                    break;
                case dal_data_provider.odbc:
                    for (int ix = 0; ix < params_count; ++ix)
                    {
                        idb_params[ix] = new OdbcParameter();
                    }
                    break;
                //case dal_data_provider.oracle:
                //    for (int ix = 0; ix < params_count; ++ix)
                //    {
                //        idb_params[ix] = new OracleParameter();
                //    }
                //    break;
                default:
                    idb_params = null;
                    break;
            }
            return idb_params;
        }
    }
    /// <summary>
    /// Implementation of IDALManager Interface
    /// </summary>
    public sealed class dal_manager : IDALManager, IDisposable
    {
        private IDbConnection idb_connection1;
        private IDataReader idata_reader;
        private IDbCommand _idb_command1;
        private dal_data_provider _dal_data_provider1;
        private IDbTransaction idb_transaction1 = null;
        private string connection;
        public dal_manager()
        {
            this._dal_data_provider1 = dal_data_provider.sql_server;
            this.connection = ConfigurationManager.AppSettings["conStr"].ToString();
            //this.connection = "Data Source=" + ConfigurationManager.AppSettings["serverName"] + ";Initial Catalog=" + ConfigurationManager.AppSettings["dbName"] + ";Persist Security Info=True;User ID=" + ConfigurationManager.AppSettings["userId"] + ";pwd=" + ConfigurationManager.AppSettings["passWord"];
        }

        public dal_manager(string secondDB)
        {
            this._dal_data_provider1 = dal_data_provider.sql_server;
            this.connection = Convert.ToString(ConfigurationManager.AppSettings["conStr2ndDB"]);
        }

        public dal_manager(dal_data_provider dal_data_provider1)
        {
            this._dal_data_provider1 = dal_data_provider1;
        }
        public dal_manager(dal_data_provider dal_data_provider1, string connectionString)
        {
            this._dal_data_provider1 = dal_data_provider1;
            this.connection = connectionString;
        }
        public IDbConnection Connection
        {

            get { return idb_connection1; }
        }
        public IDataReader idata_reader1
        {
            get { return idata_reader; }
            set { idata_reader = value; }
        }
        public dal_data_provider dal_data_provider
        {
            get { return _dal_data_provider1; }
            set { _dal_data_provider1 = value; }
        }
        public string connection_string
        {
            get { return connection; }
            set { connection = value; }
        }
        public IDbCommand idb_command1
        {
            get { return idb_command1; }
        }
        public IDbTransaction Transaction
        {
            get { return idb_transaction1; }
        }
        /// <summary>
        /// Implementation of begin_transaction method
        /// </summary>
        public void begin_transaction()
        {

            initialize();
            if (this.idb_transaction1 == null)
                this.idb_transaction1 = idb_connection1.BeginTransaction();
            this._idb_command1.Transaction = idb_transaction1;

        }
        /// <summary>
        /// Implementation of commit_transaction method
        /// </summary>
        public void commit_transaction()
        {
            if (this.idb_transaction1 != null)
                this.idb_transaction1.Commit();
            this.idb_transaction1 = null;
        }
        /// <summary>
        /// Implementation of rollback_transaction method
        /// </summary>
        public void rollback_transaction()
        {
            if (this.idb_transaction1 != null)
                this.idb_transaction1.Rollback();
            this.idb_transaction1 = null;
        }
        /// <summary>
        ///  Implementation of initialize method
        /// </summary>
        public void initialize()
        {
            try
            {
                if (idb_connection1 == null)
                {
                    this.idb_connection1 = dal_manager_factory.get_connection(this._dal_data_provider1);
                    this.idb_connection1.ConnectionString = connection;

                    if (idb_connection1.State != ConnectionState.Open)
                        this.idb_connection1.Open();
                    this._idb_command1 = dal_manager_factory.get_command(this._dal_data_provider1);
                    _idb_command1.Connection = this.Connection;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        ///  Implementation of Close method
        /// </summary>
        public void Close()
        {
            if (idb_connection1.State != ConnectionState.Closed)
                idb_connection1.Close();
        }
        /// <summary>
        ///  Implementation of Dispose method
        /// </summary>
        public void Dispose()
        {
            GC.SuppressFinalize(this);
            this.Close();
            this._idb_command1 = null;
            this.idb_transaction1 = null;
            this.idb_connection1 = null;
        }
        /// <summary>
        ///  Implementation of execute_reader method
        /// </summary>
        /// <param name="dal_stored_procedure1"></param>
        /// <returns></returns>
        public IDataReader execute_reader(dal_stored_procedure dal_stored_procedure1)
        {
            try
            {
                prepare_command(dal_stored_procedure1);
                this.idata_reader1 = _idb_command1.ExecuteReader(CommandBehavior.CloseConnection);
                reset_command(dal_stored_procedure1);
                _idb_command1.Parameters.Clear();
                return this.idata_reader1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        ///  Implementation of close_reader method
        /// </summary>
        public void close_reader()
        {
            if (this.idata_reader1 != null)
                this.idata_reader1.Close();
        }
        /// <summary>
        ///  Implementation of prepare_command method
        /// </summary>
        /// <param name="dal_stored_procedure1"></param>
        public void prepare_command(dal_stored_procedure dal_stored_procedure1)
        {
            initialize();

            this._idb_command1.CommandText = dal_stored_procedure1.name;
            this._idb_command1.CommandType = CommandType.StoredProcedure;
            if (Transaction != null)
            {
                this._idb_command1.Transaction = Transaction;
            }
            dal_params dal_params = dal_stored_procedure1.get_params();
            IDbDataParameter[] idb_data_parameter1 = dal_manager_factory.get_parameters(this._dal_data_provider1, dal_params.Count);
            foreach (dal_param parameter in dal_stored_procedure1.get_params())
            {
                int current_index = dal_params.IndexOf(parameter);
                idb_data_parameter1[current_index].ParameterName = parameter.param_name;
                idb_data_parameter1[current_index].DbType = parameter.param_data_type;

                if (parameter.param_value == null || parameter.param_value.Equals(DBNull.Value))
                {
                    idb_data_parameter1[current_index].Value = DBNull.Value;
                }
                else
                {
                    DateTime date_time = new DateTime();

                    if (parameter.param_value.Equals(date_time))
                    {
                        idb_data_parameter1[current_index].Value = DBNull.Value;
                    }
                    else
                    {
                        idb_data_parameter1[current_index].Value = parameter.param_value;
                    }
                }


                idb_data_parameter1[current_index].Direction = parameter.param_direction;
                this._idb_command1.Parameters.Add(idb_data_parameter1[current_index]);
            }
        }
        /// <summary>
        ///  Implementation of reset_command method
        /// </summary>
        /// <param name="dal_stored_procedure1"></param>
        public void reset_command(dal_stored_procedure dal_stored_procedure1)
        {

            dal_params dal_params = dal_stored_procedure1.get_params();
            foreach (IDbDataParameter parameter in _idb_command1.Parameters)
            {
                if (parameter.Direction != ParameterDirection.Input)
                {

                    int current_index = _idb_command1.Parameters.IndexOf(parameter);
                    dal_param dal_param = dal_params[current_index];

                    dal_param.param_value = parameter.Value;
                    dal_params[current_index] = dal_param;
                }
            }
        }
        /// <summary>
        ///  Implementation of execute_nonquery method
        /// </summary>
        /// <param name="dal_stored_procedure1"></param>
        /// <returns></returns>
        public int execute_nonquery(dal_stored_procedure dal_stored_procedure1)
        {

            try
            {
                prepare_command(dal_stored_procedure1);
                int returnValue = _idb_command1.ExecuteNonQuery();

                reset_command(dal_stored_procedure1);
                return returnValue;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                this.Dispose();
            }
            // _idb_command1.Parameters.Clear(); Commented for the time being(Maya)

        }
        /// <summary>
        ///  Implementation of execute_scalar method
        /// </summary>
        /// <param name="dal_stored_procedure1"></param>
        /// <returns></returns>
        public object execute_scalar(dal_stored_procedure dal_stored_procedure1)
        {
            try
            {
                prepare_command(dal_stored_procedure1);
                object returnValue = _idb_command1.ExecuteScalar();
                _idb_command1.Parameters.Clear();
                return returnValue;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                this.Dispose();
            }
        }
        /// <summary>
        ///  Implementation of execute_dataset method
        /// </summary>
        /// <param name="dal_stored_procedure1"></param>
        /// <returns></returns>
        public DataSet execute_dataset(dal_stored_procedure dal_stored_procedure1)
        {
            try
            {
                prepare_command(dal_stored_procedure1);
                IDbDataAdapter IDbDataAdapter1 = dal_manager_factory.get_data_adapter(this._dal_data_provider1); _idb_command1.CommandTimeout = 600;
                IDbDataAdapter1.SelectCommand = _idb_command1;
                DataSet dataSet = new DataSet();
                IDbDataAdapter1.Fill(dataSet);
                _idb_command1.Parameters.Clear();
                return dataSet;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                this.Dispose();
            }
        }
    }

}