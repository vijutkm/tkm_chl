﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TKM_CENTRE_FOR_HIGHER_LEARNING.Models
{
    public class ReportSalaryModel
    {
        [DisplayName("Staff")]
        [Required(ErrorMessage = "From date is required.")]
        public int staff_id { get; set; }

        [DisplayName("Month")]
        [Required(ErrorMessage = "Month is required.")]
        public int month { get; set; }

        [DisplayName("year")]
        [Required(ErrorMessage = "Year is required.")]
        public int year { get; set; }

     

    }
}