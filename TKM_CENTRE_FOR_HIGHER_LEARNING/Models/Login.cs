﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web;


namespace TKM_CENTRE_FOR_HIGHER_LEARNING.Models
{
    public class Login
    {
        public int UserId { get; set; }

        [Required]
        public string UserName { get; set; }
        [Required]
        public string Password { get; set; }


      

        public int Validateusers(Login logobj)
        {
            int return_value = 0;
            try
            {
                dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
                dal_stored_procedure1.name = "[dbo].[pr_validate_users]";
                dal_manager dal_manager1 = new dal_manager();
                dal_stored_procedure1.add_parameter("@user_name", logobj.UserName, DbType.String, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@password", logobj.Password, DbType.String, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@return_val", null, DbType.Int32, ParameterDirection.Output);

                dal_manager1.execute_nonquery(dal_stored_procedure1);
                return_value = (int)dal_stored_procedure1.get_params()["@return_val"].param_value;
                return return_value;

            }
            catch (Exception ex)
            {

            }
            return return_value;
        }

        public int log_users(Login logobj,string ip)
        {
            int return_value = 0;
            try
            {
                dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
                dal_stored_procedure1.name = "[dbo].[pr_log_users]";
                dal_manager dal_manager1 = new dal_manager();
                dal_stored_procedure1.add_parameter("@user_name", logobj.UserName, DbType.String, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@password", logobj.Password, DbType.String, ParameterDirection.Input);
                dal_stored_procedure1.add_parameter("@ip", ip, DbType.String, ParameterDirection.Input);
                dal_manager1.execute_nonquery(dal_stored_procedure1);
  
            }
            catch (Exception ex)
            {
              return  return_value = -1;
            }
            return return_value;
        }


        public class forgetpasswords
        {
            [Required]
            public string mob_number { get; set; }

           [Required]
            public int otp { get; set; }

            public int Validate_number(forgetpasswords fobj)
            {
                int return_value = 0;
                try
                {
                    dal_stored_procedure dal_stored_procedure1 = new dal_stored_procedure();
                    dal_stored_procedure1.name = "[dbo].[pr_check_mob_number]";
                    dal_manager dal_manager1 = new dal_manager();
                    dal_stored_procedure1.add_parameter("@mob_number", fobj.mob_number, DbType.String, ParameterDirection.Input);

                    dal_stored_procedure1.add_parameter("@return_val", null, DbType.Int32, ParameterDirection.Output);

                    dal_manager1.execute_nonquery(dal_stored_procedure1);
                    return_value = (int)dal_stored_procedure1.get_params()["@return_val"].param_value;
                    return return_value;

                }
                catch (Exception ex)
                {

                }
                return return_value;
            }
        }




        public class password_change
        {
            [Required]
            public string new_password { get; set; }

            [Required]
            public string confirm_password { get; set; }
        }


    }
}