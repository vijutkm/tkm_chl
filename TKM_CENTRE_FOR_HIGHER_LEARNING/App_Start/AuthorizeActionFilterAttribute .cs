﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace TKM_CENTRE_FOR_HIGHER_LEARNING.App_Start
{
    public class AuthorizeActionFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpSessionStateBase session = filterContext.HttpContext.Session;
            Controller controller = filterContext.Controller as Controller;
           controller.ToString();
            HttpContext ctx = HttpContext.Current;

            if (controller != null)
            {
                if (controller.ToString().Contains("Login") || controller.ToString().Contains("Forgotpassword"))
                {

                }
                else
                {
                    if (session["UserID"] == null)
                    {
                        ctx.Response.Redirect("~/Login/Login");
                        // filterContext.Cancel = true;
                        //  new RedirectToRouteResult(new RouteValueDictionary { { "action", "Login" }, { "controller", "Login" } });
                    }
                }
            }

            base.OnActionExecuting(filterContext);
        }


    }
    
    
}