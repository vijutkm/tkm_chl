﻿using System.Web;
using System.Web.Mvc;
using TKM_CENTRE_FOR_HIGHER_LEARNING.App_Start;

namespace TKM_CENTRE_FOR_HIGHER_LEARNING
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new AuthorizeActionFilterAttribute());
        }
    }
}
