﻿using System.Web;
using System.Web.Optimization;

namespace TKM_CENTRE_FOR_HIGHER_LEARNING
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            //bundles.Add(new ScriptBundle("~/bundles/datatableJs").Include(
            //          "~/Scripts/jquery-3.3.1.js"
            //         ));
            bundles.Add(new ScriptBundle("~/bundles/jquerydataTables").Include(                  
                    "~/js/jquery.dataTables.min.js"
                   ));
            bundles.Add(new ScriptBundle("~/bundles/dataTablesbootstrap4").Include(               
                  "~/js/dataTables.bootstrap4.min.js"));

            bundles.Add(new StyleBundle("~/bundles/bootstrap4css").Include(
                     "~/css/dataTables.bootstrap4.min.css"));

            bundles.Add(new StyleBundle("~/bundles/bootstrapcss").Include(
                  "~/css/bootstrap.css"));
            //js  
            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                      "~/Scripts/jquery-ui-1.12.1.js"));

            //css  
            bundles.Add(new StyleBundle("~/Content/cssjqryUi").Include(
                   "~/Content/themes/base/jquery-ui.css"));
        }
    }
}
