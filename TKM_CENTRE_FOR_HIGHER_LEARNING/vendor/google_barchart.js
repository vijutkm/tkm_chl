﻿
$.getJSON('/Payment/payment_details/', function (data) {
  
    var Debit;
    var Credit;
   
    $.each(data, function (i, country) {
        Credit = country.Credit;
        Debit = country.Debit;
    });
    google.charts.load('current', { packages: ['corechart', 'bar'] });
    google.charts.setOnLoadCallback(drawBarColors);

    function drawBarColors() {
        $.getJSON('/Payment/payment_details/', function (data) {

            $.each(data, function (i, country) {
                Debit = country.Debit;
                Credit = country.Credit;
            });

        });    
        var data = google.visualization.arrayToDataTable([
        ['', '', { role: 'style' }, { role: 'annotation' }],
        ['Debit', Debit, '#b0120a', Debit],
        ['Credit', Credit, 'Green', Credit]
    
        ]);

        var options = {
            title: 'Transaction summary',
            chartArea: { width: '80%' },
           
            hAxis: {
                title: 'In Rs.',
                minValue: 0
            },
            vAxis: {
                title: ''
            }
        };
        var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
        chart.draw(data, options);
    }



});

