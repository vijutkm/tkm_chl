﻿$.getJSON('/Payment/payment_details_chart/', function (data) {
  
    var Amount;
    var category_name;
    var d = [['Item', 'Amount']]; 
    $.each(data, function (i, country) {
        Amount = country.Amount;
        category_name = country.category_name;
        d.push([category_name, Amount]);
    });

    google.charts.load("current", { packages: ["corechart"] });
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
        var data = google.visualization.arrayToDataTable(d);

        var options = {
            chartArea: { top: '10%', width: '100%', height: '85%' },
            title: 'Income And Expence',
            legend: 'none',
            pieSliceText: 'label',
            pieHole: 0.37,
            pieSliceBorder: 100,
            animation: {
                duration: 1000,
                easing: 'in',
                startup: true
            },
            slices: {
                4: { offset: 0.2 },
                8: { offset: 0.6 },
                12: { offset: 0.3 },
                14: { offset: 0.4 },
                15: { offset: 0.5 },
            }
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));
        chart.draw(data, options);
        // initial value
        var percent = 0;
        // start the animation loop
        var handler = setInterval(function () {
            // values increment
            percent += 1;
            // apply new values
            data.setValue(0, 1, percent);
            data.setValue(1, 1, 100 - percent);
            // update the pie
            chart.draw(data, options);
            // check if we have reached the desired value
            if (percent > 34)
                // stop the loop
                clearInterval(handler);
        }, 30);
      
}
});