// Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#292b2c';

var a = $("#ID option:selected").val();
var url = "/Fee/TellMeDate/"+a;
//$.getJSON(url, a, function (data) {
$.getJSON('/Fee/Fee_paid_details/' + a, function (data) {
    var created_on;
    var Fee;
    var Paid;
    var Balance;
    $.each(data, function (i, country) {
        created_on = country.created_on;
        Paid = country.Amount;
        Fee = country.Fee;
        Balance = country.Balance;
    });

    if (created_on === null)
    {
        $('#last_update').html('No details' );
        $('#success_div').html('No details' );
    }
    else    
    {
        $('#last_update').html('Updated on ' + created_on);
        $('#success_div').html('Last payment done on ' + created_on);
    }
    $('#total_fee_div').html('Total fee amount is ' + Fee);
    $('#amount_paid_div').html('Paid amount is ' + Paid);
    $('#balance_fee_div').html('Amount need to pay is ' + Balance);

    viewChart(Fee, Paid, Balance);
});

function viewChart(Fee, Paid, Balance) {
    var ctx = document.getElementById("myPieChart");
    var myPieChart = new Chart(ctx, {
        type: 'pie',
        data: {
            labels: ["Total Fee", "Paid", "Due"],
            datasets: [{
                data: [Fee, Paid, Balance],
                backgroundColor: ['#007bff', '#dc3545', '#ffc107'],
            }],
        },
    });
}
