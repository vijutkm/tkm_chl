am4core.ready(function () {

    // Themes begin
    am4core.useTheme(am4themes_animated);
    // Themes end

    var chart = am4core.create("chartdiv", am4charts.XYChart);
    chart.titles.create().text = "   Visitor Count And Failed Access";
    chart.data = [{
        "country": "Jan",
        "visits": 1025,
        "error": 100
    }, {
        "country": "Feb",
        "visits": 1482,
        "error": 180
    }, {
        "country": "Mar",
        "visits": 609,
        "error": 130
    }, {
        "country": "Apr",
        "visits": 822,
        "error": 200
    }, {
        "country": "May",
        "visits": 122,
        "error": 50
    }, {
        "country": "Jun",
        "visits": 1114,
        "error": 110
    }, {
        "country": "July",
        "visits": 984,
        "error": 120
    },
     {
         "country": "Aug",
         "visits": 784,
         "error": 20
     }];

    chart.padding(10, 10, 10, 10);

    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.renderer.grid.template.location = 0;
    categoryAxis.dataFields.category = "country";
    categoryAxis.renderer.minGridDistance = 60;

    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

    var series = chart.series.push(new am4charts.LineSeries());
    series.dataFields.categoryX = "country";
    series.dataFields.valueY = "visits";
    series.tooltipText = "{valueY.value} Invalid:{error}"

    var errorBullet = series.bullets.create(am4charts.ErrorBullet);
    errorBullet.isDynamic = true;
    errorBullet.strokeWidth = 2;

    var circle = errorBullet.createChild(am4core.Circle);
    circle.radius = 3;
    circle.fill = am4core.color("#ffffff");

    // adapter adjusts height of a bullet
    errorBullet.adapter.add("pixelHeight", function (pixelHeight, target) {
        var dataItem = target.dataItem;

        if (dataItem) {
            var value = dataItem.valueY;
            var errorTopValue = value + dataItem.dataContext.error / 2;
            var errorTopY = valueAxis.valueToPoint(errorTopValue).y;

            var errorBottomValue = value - dataItem.dataContext.error / 2;
            var errorBottomY = valueAxis.valueToPoint(errorBottomValue).y;

            return Math.abs(errorTopY - errorBottomY);
        }
        return pixelHeight;
    })

    chart.cursor = new am4charts.XYCursor();

}); // end am4core.ready()